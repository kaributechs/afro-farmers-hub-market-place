const {ADD_CONTACTDETAILS_DATA} = require("../types/businessInsurance-types")

const initialContactState = {
  formdata: {
        contactDetails_clientEmailAddress: "",
        contactDetails_clientPhoneNumber: "",
        contactDetails_clientResidentialStreetAddress: "",
        contactDetails_clientResidentialCity: "",
        contactDetails_clientRegion: "",
        contactDetails_residentialZip: "",
        contactDetails_workAddressZip: "",
        contactDetails_clientCountry: "",
        contactDetails_clientWorkStreetAddress: "",
        contactDetails_clientWorkCity: "",
        contactDetails_workRegion: "",
        contactDetails_workCountry: "",

  }
}

const contactDetailsReducer = (state = initialContactState, action) =>{
    switch(action.type){
        case ADD_CONTACTDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default contactDetailsReducer



