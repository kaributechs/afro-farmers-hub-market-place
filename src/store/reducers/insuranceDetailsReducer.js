const {ADD_INSURANCEDETAILS_DATA} = require("../types/businessInsurance-types")

const initialInsuranceState = {
  formdata: {
  insuranceQuestions_carInsuranceType: "",
  insuranceQuestions_thirdPartyLiability: "",
  insuranceQuestions_allRisksYesOrNo: "",
  insuranceQuestions_replacementCarYesOrNo: "",
  insuranceQuestions_periodOfInsuranceFrom: "",
  insuranceQuestions_periodOfInsuranceTo: "",

  }
}

const insuranceDetailsReducer = (state = initialInsuranceState, action) =>{
    switch(action.type){
        case ADD_INSURANCEDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default insuranceDetailsReducer



