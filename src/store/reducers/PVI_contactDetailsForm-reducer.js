const {ADD_CONTACTDETAILS_DATA} = require("../types/personalInsuranceApp-types")

const initialContactState = {
    formData:{
        clientEmailAddress:"",
        clientPhoneNumber:"",
        clientResidentialStreetAddress:"",
        clientResidentialCity:"",
        clientRegion:"",
        residentialZip:"",
        workAddressZip:"",
        clientCountry:"",
        clientWorkStreetAddress:"",
        clientWorkCity:"",
        workRegion:"",
        workCountry:"",
    }
}

const contactDetailsFormReducer = (state = initialContactState, action) =>{
    switch(action.type){
        case ADD_CONTACTDETAILS_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default contactDetailsFormReducer