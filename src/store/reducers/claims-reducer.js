import {CLAIM_Available_Tasks} from "../types/claims-types";

const initialState = {
  tasks: []
}

export default function   (state = initialState, action )  {
  switch (action.type) {
    case CLAIM_Available_Tasks:
      return { ...state,
        tasks: action.payload
      }
    default:
      return state
  }
}
