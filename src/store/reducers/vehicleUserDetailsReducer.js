const {ADD_DRIVERDETAILS_DATA} = require("../types/businessInsurance-types")

const initialUserState = {
  formdata: {
    vehicleUserDetails_LastName: "",
    vehicleUserDetails_FirstName: "",
    vehicleUserDetails_LicenseNumber: "",
    vehicleUserDetails_NationalId: "",
    vehicleUserDetails_DateOfBirth: "",
    vehicleUserDetails_EmailAddress: "",
    vehicleUserDetails_PhoneNumber: "",
    vehicleUserDetails_Gender: "",
    vehicleUserDetails_Role: "",
    vehicleUserDetails_RoleDescription: "",
    vehicleUserDetails_MaritalStatus: "",

  }
}

const vehicleUserDetailsReducer = (state = initialUserState, action) =>{
    switch(action.type){
        case ADD_DRIVERDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default vehicleUserDetailsReducer



