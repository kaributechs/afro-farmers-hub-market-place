const { ADD_DRIVERSFORM_DATA } = require("../types/car-claim-form-types")

const initialDriversState = {
    formdata: {
        DriversDetails: {
            driversFirstName:"",
            driversLastName:"",
            driversEmail:"",
            driversPhoneNumber:"",
            driversResidentialStreetAddress:"",
            driversResidentialCity:"",
            driversRegion:"",
            driversCountry:"",
            driversZipCode:"",
            driversWorkingStreetAddress:"",
            driversWorkingCity:"",
            driversWorkingRegion:"",
            driversWorkingCountry:"",
            driversWorkingZipCode:"",
            driversLicenseNumber:"",
            dateOfLicenseIssue:""
        }
    }

}

const driversFormReducer = (state = initialDriversState, action) => {
    switch (action.type) {
        case ADD_DRIVERSFORM_DATA: return {
            ...state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default driversFormReducer

