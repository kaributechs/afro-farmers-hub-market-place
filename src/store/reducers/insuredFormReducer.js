const { ADD_INSUREDFORM_DATA } = require("../types/car-claim-form-types")

const initialInsuredState = {
    formdata: {
        InsuredDetails:{
            insuredFirstName:"",
            insuredLastName:"",
            insuredPhoneNumber:"",
            insuredEmail:"",
            insuredResidentialStreetAddress:"",
            insuredResidentialCity:"",
            insuredRegion:"",
            insuredCountry:"",
            insuredZipCode:"",
            insuredWorkingStreetAddress:"",
            insuredWorkingCity:"",
            insuredWorkingRegion:"",
            insuredWorkingCountry:"",
            insuredWorkingZipCode:"",

        }
     
    }
}

const insuredFormReducer = (state = initialInsuredState, action) =>{
    switch(action.type){
        case ADD_INSUREDFORM_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default insuredFormReducer

