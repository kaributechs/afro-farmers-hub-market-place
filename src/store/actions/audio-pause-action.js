import { AUDIO_PAUSE } from "../types/audio-types"

// sets current audio    
export const pauseAudioTrack = (formdata) => (dispatch) => {
    dispatch({
        type: AUDIO_PAUSE,
        payload: formdata,
        
      });
}
