import { AUDIO_SELECT } from "../types/audio-types"

// sets current audio    
export const setAudioTrack = (formdata) => (dispatch) => {
    dispatch({
        type: AUDIO_SELECT,
        payload: formdata,
        
      });
}
