import { ADD_SECONDARYDRIVERDETAILS_DATA } from "../types/personalInsuranceApp-types"

export const secondaryDriverDetailsTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_SECONDARYDRIVERDETAILS_DATA,
          payload: formdata,
          
        });

  
  }