import { ADD_INSURANCEQUESTIONS_DATA } from "../types/personalInsuranceApp-types"

export const insuranceQuestionsTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_INSURANCEQUESTIONS_DATA,
          payload: formdata,
          
        });

  
  }