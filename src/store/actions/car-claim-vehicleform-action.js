import { ADD_VEHICLEFORM_DATA } from "../types/car-claim-form-types"

export const vehicleFormTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_VEHICLEFORM_DATA,
          payload: formdata,
          
        });

  
  }
  
  