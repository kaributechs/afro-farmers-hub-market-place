import React from 'react'
import CIcon from '@coreui/icons-react'
import { NavLink } from 'react-router-dom'

const _nav = [
  {
    _component: 'CNavItem',
    as: NavLink,
    anchor: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },

  {
    _component: 'CNavGroup',
    as: NavLink,
    anchor: 'Suppliers',
    to: '/claims',
    icon: <CIcon name="cil-puzzle" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Claims',
        to: '/claims',
      },

     ],
  },

  {
    _component: 'CNavGroup',
    as: NavLink,
    anchor: 'Products',
    to: '/claims',
    icon: <CIcon name="cil-puzzle" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Claims',
        to: '/claims',
      },

     ],
  },

        {
          _component: 'CNavItem',
          as: NavLink,
          anchor: 'Logout',
          to: '/claims',
        },

]

export default _nav
