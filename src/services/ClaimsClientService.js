import {HttpService} from "./HttpService";

export class ClaimsClientService extends HttpService {

  fetchAvailableTasks  () {
    return this.get(`/api/v1/tasks/my-tasks`);
  }

//Handle Approve Task
  handleApproveTask (event)  {
    return this.post(`/api/v1/task/${event.id}/complete`, {
      approved: true,
      autoAssess: true
    });
  }

//Get More Details Task
  handleMoreDetailsTask (event)  {
    return this.post(`/api/v1/task/${event.id}/complete`, {
      getMoreDetails: true
    });
  }

//Handle Reject Task
  handleRejectedTask (event) {
    return this.post(`/api/v1/task/${event.id}/complete`, {
      approved: false
    });
  }
}
