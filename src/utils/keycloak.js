import Keycloak from 'keycloak-js'

const keycloak = new Keycloak({
    realm: "InsureHub", // realm as configured in Keycloak
    url: "http://insurehubbackend.southafricanorth.cloudapp.azure.com:8180/auth/", // URL of the Keycloak server
    clientId: "Insurehub-React-Client-Project", // client id as configured in the realm in Keycloak
  })

export default keycloak
