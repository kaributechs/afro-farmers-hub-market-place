import React from 'react';
import PropTypes from 'prop-types';
import useTrans from '../../hooks/useTrans';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

const  DefaultFooter =(props)=> {

  // eslint-disable-next-line
  const[t, handleClick] = useTrans();
  

    // eslint-disable-next-line
    const { children, ...attributes } = props;

    return (
      <React.Fragment>
        <span><a href="website.com">InsureHub</a> &copy; 2021 InsureHub.</span>
        <span className="ml-auto">{t("Poweredby.1")} <a href="website.com">InsureHub</a></span>
      </React.Fragment>
    );
  
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
