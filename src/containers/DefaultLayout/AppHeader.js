import React from 'react'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import { AppBreadcrumb } from './index'
import { AppHeaderDropdown } from './header/index'
import { useTranslation } from 'react-i18next'
import useKeyCloak from "../../hooks/useKey";
import {layoutSideBarShow} from "../../store/actions";

const AppHeader = () => {
  const dispatch = useDispatch();
  const [t] = useTranslation();

  const [keycloak, isAuthenticated] = useKeyCloak();
  const sidebarShow = useSelector((state) => state.layout?.sidebarShow)

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        <CHeaderToggler
          className="ms-md-3 d-lg-none"
          onClick={() => dispatch(layoutSideBarShow(!sidebarShow))}
        >
          <CIcon name="cil-menu" size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" >
               <CIcon name="logo" height="48" alt="Logo" />
        </CHeaderBrand>
        <CHeaderNav className="d-none d-md-flex me-auto">
          <CNavItem>
            <CNavLink to="/dashboard" component={NavLink} activeClassName="active">
              {t("Claims")}
            </CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink  to="/users"  component={NavLink} >{t("Premium")}</CNavLink>
          </CNavItem>
          <CNavItem >
            <CNavLink to="#"  component={NavLink}>{t("Quotation")}</CNavLink>
          </CNavItem>
          <CNavItem >
            <CNavLink to="#"  component={NavLink}>{t("Rewards")}</CNavLink>
          </CNavItem>
          <CNavItem >
            <CNavLink to="#"  component={NavLink}>{t("Underwriting")}</CNavLink>
          </CNavItem>
          <CNavItem >
            <CNavLink to="#"  component={NavLink}>{t("Service")}</CNavLink>
          </CNavItem>
          <CNavItem >
            <CNavLink to="#"  component={NavLink}>{t("Commissions")}</CNavLink>
          </CNavItem>
        </CHeaderNav>

        <CHeaderNav className="ms-3">
          <AppHeaderDropdown />
        </CHeaderNav>
      </CContainer>
      <CHeaderDivider />
      <CContainer fluid>
        <AppBreadcrumb />
      </CContainer>
    </CHeader>
  )
}

export default AppHeader
