import React from 'react'
import { CFooter } from '@coreui/react'
import useTrans from "../../hooks/useTrans";

const AppFooter = () => {
  const[t, handleClick] = useTrans();
  return (
    <CFooter>
      <div>
        <span><a href="website.com">InsureHub</a> &copy; 2021 InsureHub.</span>
      </div>
      <div className="ms-auto">
        <span className="ml-auto">{t("Poweredby")} <a href="website.com">InsureHub</a></span>
      </div>
    </CFooter>

  )
}

export default React.memo(AppFooter)
