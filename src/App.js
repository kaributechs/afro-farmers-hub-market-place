import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

// import { renderRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import './scss/style.scss'
import {ReactKeycloakProvider} from '@react-keycloak/web'
import keycloak from './utils/keycloak'
const Loading = () => <div className="animated fadeIn pt-3 text-center"><div className="sk-spinner sk-spinner-pulse"> < /div></div >;

const loading = () => <div className="animated fadeIn pt-3 text-center"><div className="sk-spinner sk-spinner-pulse"> < /div></div >;

// Containers
const DefaultLayout = Loadable({
    loader: () => import ('./containers/DefaultLayout'),
    loading
});

// Pages
const Login = Loadable({
    loader: () => import ('./pages/login/Login'),
    loading
});

const Register = Loadable({
    loader: () => import ('./pages/register/Register'),
    loading
});

const Page404 = Loadable({
    loader: () => import ('./pages/page404/Page404'),
    loading
});

const Page500 = Loadable({
    loader: () => import ('./pages/page500/Page500'),
    loading
});


const successLogin = (event) => {
    window.accessToken = event.token
}


const App = () => {
    return (<ReactKeycloakProvider authClient={keycloak}
        initOptions={
            {onLoad: "login-required"}
        }
        onTokens={successLogin}

        LoadingComponent={<Loading/>}>

        <Router>
            <Switch>
                <Route exact path="/login" name="Login Page"
                    component={Login}/>
                <Route exact path="/register" name="Register Page"
                    component={Register}/>
                <Route exact path="/404" name="Page 404"
                    component={Page404}/>
                <Route exact path="/500" name="Page 500"
                    component={Page500}/>
                <Route path="/" name="Home"
                    component={DefaultLayout}/>
            </Switch>
        </Router>
    </ReactKeycloakProvider>)
}


export default App;
