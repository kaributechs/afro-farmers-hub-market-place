import React from 'react';
import { useFormikContext } from 'formik';
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { CFormLabel } from '@coreui/react';
import { Typography, Grid } from '@material-ui/core';
import {InsuredForm,VehicleForm,DriversForm,QuestionsForm} from "./Forms";

 const ReviewForm = (props) => {
  const {
    formField: {
      screenId,
    InsuredDetails:{
     insuredId,
     insuredSalutation,
     insuredFirstName,
     insuredLastName,
     insuredPhoneNumber,
     insuredEmail,
     insuredResidentialStreetAddress,
     insuredResidentialCity,
     insuredRegion,
     insuredCountry,
     insuredZipCode,
     insuredWorkingStreetAddress,
     insuredWorkingCity,
     insuredWorkingRegion,
     insuredWorkingCountry,
     insuredWorkingZipCode}, 
  
    VehicleDetails:{
     vehicleMake,
     vehicleModel,
     vehicleModelYear,
     vehicleRegistrationNumber,
     vehicleMilage,
     vehicleUsage
      },
      
  
    DriversDetails:{
      driversFirstName,
      driversLastName,
      driversEmail,
      driversPhoneNumber,
      driversResidentialStreetAddress,
      driversResidentialCity,
      driversRegion,
      driversCountry,
      driversZipCode,
      driversWorkingStreetAddress,
      driversWorkingCity,
      driversWorkingRegion,
      driversWorkingCountry,
      driversWorkingZipCode,
      driversLicenseNumber,
      dateOfLicenseIssue
      },
  
      FinalPageDetails:{
      driversCondition,
      accidentHistory,
      accidentHistoryDetails,
      driversConditionDetails,
      vehiclePurpose, 
      natureOfInjuries,
      accidentDescription,
  
      },
    }
    
  }= props;
  
  screenId.name='Review_Form_Page'

  const { values: formValues } = useFormikContext();
  return (

    <div className="animated fadeIn">
     <FormGroup row className="my-0">

     <Col xs="12" sm="6" lg="6">
    <CFormLabel style={{fontSize:'17px',color:'gray',fontWeight:'bold'}}for='Summary'>Summary</CFormLabel>
    </Col>
    </FormGroup>

    <FormGroup row className="my-0">
    <Col xs="12" sm="6" lg="6">
    <span style={{fontSize:'16px',color:'gray'}}>Insured's Details</span>
    {/* <InsuredForm formField = {formValues}/> */}
    </Col>

    <Col xs="12" sm="6" lg="6">
    <span style={{fontSize:'16px',color:'gray'}}>Vehicle Details</span>
    </Col>

    </FormGroup>

    

    <FormGroup row className="my-0">
    <span style={{paddingBlock:'60px'}}></span>
    <Col xs="12" sm="6" lg="6">
    <span style={{fontSize:'16px',color:'gray'}}>Drivers's Details</span>
    </Col>

    <Col xs="12" sm="6" lg="6">
    <span style={{fontSize:'16px',color:'gray'}}>Insurance Questions</span>
    </Col>

    </FormGroup>

    </div>

  );
}
export default ReviewForm
