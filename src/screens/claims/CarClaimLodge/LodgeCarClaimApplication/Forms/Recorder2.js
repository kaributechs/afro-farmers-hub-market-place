import React, { Component } from "react";

import AudioReactRecorder, { RecordState } from "audio-react-recorder";

class Recorder2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      recordState: null
    };
  }

  start = () => {
    this.setState({
      recordState: RecordState.START
    });
  };

  stop = () => {
    this.setState({
      recordState: RecordState.STOP
    });
  };

  pause = () => {
    this.setState({
      recordState: RecordState.PAUSE
    });
  };

  //audioData contains blob and blobUrl
  //you can use this function to pass the blob staight away
  //audioData.blob instead of just audioData to get a hold of the blob
  onStop = (audioData) => {
    console.log("audioData", audioData);
  };

  render() {
    const { recordState } = this.state;

    return (
      <div>
        <span style={{fontSize:'10'}}>PRESS START TO BEGIN RECORDING </span>
        <AudioReactRecorder state={recordState} onStop={this.onStop} />

        <button onClick={this.start}>Start</button>
        <button onClick={this.stop}>Stop</button>
        <button onClick={this.pause}>Pause</button>
      </div>
    );
  }
}

export default Recorder2;
