import React, { useState } from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { useFormikContext } from 'formik';
import { CFormLabel } from "@coreui/react";
import { InputSelect, InputField } from 'src/screens/applications/FormFields';
import Select from 'react-select'
const vehicleusage = [
  { text: "Select vehicle usage type", value: "" },
  { text: "Personal", value: "personal" },
  { text: "Taxi", value: "Taxi" },
  { text: "Personal", value: "personal" },
  { text: "Company Car", value: "company" },
]

const vehiclemake = [
  { text: "Select vehicle make ", value: "" },
  { text: "ACURA", value: "ACURA" },
  { text: "ASTON MARTIN", value: "ASTON MARTIN" },
  { text: "AUDI", value: "AUDI" },
  { text: "BENTLEY", value: "BENTLEY" },
  { text: "BMW", value: "BMW" },
  { text: "BUICK", value: "BUICK" },
  { text: "CADILLAC", value: "CADILLAC" },
  { text: "CHEVROLET", value: "CHEVROLET" },
  { text: "CHRYSLER", value: "CHRYSLER" },
  { text: "DODGE", value: "DODGE" },
  { text: "FERRARI", value: "FERRARI" },
  { text: "FORD", value: "FORD" },
  { text: "GMC", value: "GMC" },
  { text: "HONDA", value: "HONDA" },
  { text: "HUMMER", value: "HUMMER" },
  { text: "HYUNDAI", value: "HYUNDAI" },
  { text: "INFINITI", value: "INFINITI" },
  { text: "ISUZU", value: "ISUZU" },
  { text: "JAGUAR", value: "JAGUAR" },
  { text: "JEEP", value: "JEEP" },
  { text: "KIA", value: "KIA" },
  { text: "LAMBORGHINI", value: "LAMBORGHINI" },
  { text: "LAND ROVER", value: "LAND ROVER" },
  { text: "LEXUS", value: "LEXUS" },
  { text: "LINCOLN", value: "LINCOLN" },
  { text: "LOTUS", value: "LOTUS" },
  { text: "MASERATI", value: "MASERATI" },
  { text: "MAYBACH", value: "MAYBACH" },
  { text: "MAZDA", value: "MAZDA" },
  { text: "MERCEDES-BENZ", value: "MERCEDES-BENZ" },
  { text: "MERCURY", value: "MERCURY" },
  { text: "MINI", value: "MINI" },
  { text: "MITSUBISHI", value: "MITSUBISHI" },
  { text: "NISSAN", value: "NISSAN" },
  { text: "PONTIAC", value: "PONTIAC" },
  { text: "PORSCHE", value: "PORSCHE" },
  { text: "ROLLS-ROYCE", value: "ROLLS-ROYCE" },
  { text: "SAAB", value: "SAAB" },
  { text: "SATURN", value: "SATURN" },
  { text: "SUBARU", value: "SUBARU" },
  { text: "SUZUKI", value: "SUZUKI" },
  { text: "TOYOTA", value: "TOYOTA" },
  { text: "VOLKSWAGEN", value: "VOLKSWAGEN" },
  { text: "VOLVO", value: "VOLVO" },
  { text: "other", value: "other" },]

  function range(start, end) {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
  };


  const years = range(1900, 2100);
  const vehicleyears= [{text:"Select Model Year",value:"Select Model Year"}];
  years.forEach(function(element) {
    vehicleyears.push({ text:element, value:element })
});

export const VehicleForm = (props) => {
  

  const {
    formField: {
      screenId,
      VehicleDetails: {
        vehicleMake,
        vehicleModel,
        vehicleModelYear,
        vehicleRegistrationNumber,
        vehicleMilage,
        vehicleUsage,
      }
    }
  } = props;
screenId.name = 'Vehicle_Form_Page'
  return (

    <div className="animated fadeIn">

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text"
              name={vehicleRegistrationNumber.name}
              id={vehicleRegistrationNumber.name}
              label={<Label htmlFor="vehicleregnum"><span style={{ color: "red"}}>*</span>Vehicle Registration Number</Label>}
              placeholder="Vehicle Registration Number"
              autoComplete="vehicle registration number"
            />
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputSelect data={vehiclemake} name={vehicleMake.name} id={vehicleMake.name}
              label={<Label htmlFor="vehiclemake"><span style={{ color: "red", size: 12 }}>*</span>Vehicle Make</Label>}
              value="value" text="text"
            />
          </FormGroup>
        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text"
              name={vehicleModel.name}
              id={vehicleModel.name}
              label={<Label htmlFor="vehiclemodel"><span style={{ color: "red", size: 12 }}>*</span>Vehicle Model</Label>}
              placeholder="Vehicle Model"
              autoComplete="vehicle-model"
            />
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputSelect data={vehicleyears} name={vehicleModelYear.name} id={vehicleModelYear.name}
              label={<Label htmlFor="vehicleyearmake"><span style={{ color: "red", size: 12 }}>*</span>Vehicle Model Year</Label>}
              value="value" text="text"></InputSelect>

          </FormGroup>
        </Col>
      </FormGroup>

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputSelect data={vehicleusage}
              name={vehicleUsage.name}
              id={vehicleUsage.name}
              label={<Label htmlFor="vehicleusage"><span style={{ color: "red", size: 12 }}>*</span>Vehicle Usage</Label>}
              value="value" text="text"></InputSelect>


          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text" name={vehicleMilage.name} id={vehicleMilage.name}
              label={<Label htmlFor="vehiclemilage"><span style={{ color: "red", size: 12 }}>*</span>Vehicle Mileage</Label>}
              placeholder="Enter vehicle mileage"
              autoComplete="Vehicle Milage"
            />


          </FormGroup>
        </Col>
      </FormGroup>


    </div>
  )

}





