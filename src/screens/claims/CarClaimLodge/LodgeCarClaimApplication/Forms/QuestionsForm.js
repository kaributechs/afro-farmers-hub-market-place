import React, { useState } from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { Field } from 'formik';
import { Link } from "react-router-dom";
import ToggleBox from "./ToggleBox";
import Audio from "./Audio";
import { InputSelect } from 'src/screens/applications/FormFields';
import ToggleBox3 from './ToggleBox3';
import UploadFiles from './UploadFiles';
import { InputField } from '../../FormFields';
import ToggleBox2 from './ToggleBox2';

export const QuestionsForm = (props) => {
  const {
    formField: {
      screenId,
      FinalPageDetails: {
        accidentHistory,
        driversCondition,
        accidentHistoryDetails,
        driversConditionDetails,
        vehiclePurpose,
        natureOfInjuries,
        accidentDescription,
      },


    },
  } = props;

  const accidentquestion = [
    { text: "Select Answer", value: "" },
    {text:"Yes", value:"Yes"},
    { text: "No", value: "No" },
  ]

  const conditionquestion = [
    { text: "Select Answer", value: "" },
    { text: "Yes", value: "Yes" },
    { text: "No", value: "No" },
  ]

  const injurytypes = [
    { text: "Specify nature of injuries", value: "" },
    { text: "No Injuries", value: "No Injuries" },
    { text: "Minor", value: "Minor" },
    { text: "Moderate", value: "Moderate" },
    { text: "Severe", value: "Severe" },

  ]
 screenId.name = 'Questions_Form_Page'
  return (

    <div className="animated fadeIn">

      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <InputSelect data={accidentquestion} name={accidentHistory.name} id={accidentHistory.name}
            label={<Label htmlFor="accidentHistory"><span style={{ color: "red", size: 12 }}>*</span>Have you been involved in any previous accidents?</Label>}
            value="value" text="text"
          > 
          </InputSelect>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputSelect data={conditionquestion} name={driversCondition.name} id={driversCondition.name}
            label={<Label htmlFor="driversCondition"><span style={{ color: "red", size: 12 }}>*</span>Do you suffer from any physical or mental defect?</Label>}
            value="value" text="text"
          >

          </InputSelect>
          
        </Col>
      </FormGroup>

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField component ="textarea" 
              name={accidentHistoryDetails.name}
              id={accidentHistoryDetails.name}
              label={<Label htmlFor="accidentHistoryDetails">Accident History Details</Label>}
              placeholder="If involved in previous accidents, give details here"
              autoComplete="If involved in previous accidents, give details here"
            />
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField component ="textarea"
              name={driversConditionDetails.name}
              id={driversConditionDetails.name}
              label={<Label htmlFor="driversConditionDetails">Specify Condition</Label>}
              placeholder="Give details if you have a condition"
              autoComplete="If driver suffers from any condition give details"
            />
          </FormGroup>
        </Col>
      </FormGroup>
     
      <hr />

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField component ="textarea"
              name={vehiclePurpose.name}
              id={vehiclePurpose.name}
              label={<Label htmlFor="vehiclePurpose"><span style={{ color: "red", size: 12 }}>*</span>Purpose of vehicle use on date of accident</Label>}
              placeholder="For what purpose was the vehicle used on the date of accident?"
              autoComplete="For what purpose was the vehicle used on the date of accident?"
            />
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputSelect data={injurytypes} name={natureOfInjuries.name} id={natureOfInjuries.name}
              label={<Label htmlFor="natureOfInjuries"><span style={{ color: "red", size: 12 }}>*</span>Nature Of Injuries</Label>}
              value="value" text="text">

            </InputSelect>
          </FormGroup>
        </Col>

      </FormGroup>
      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="12" lg="12">
          <FormGroup>
            <InputField component ="textarea"
              name={accidentDescription.name}
              id={accidentDescription.name}
              label={<Label htmlFor="accidentDescription"><span style={{ color: "red", size: 12 }}>*</span>Accident Description</Label>}
              placeholder="Accident Description"
              autoComplete="Accident Description"
            />
            <br/>
          </FormGroup>
         
        </Col>

         <FormGroup row className="my-0">

        <Col xs="12" sm="6" lg="12">
        <span style={{ paddingTop: '20x' }}>
          <i class="material-icons">headphones</i></span>
          <ToggleBox title="Audio panel">
            <Audio />
          </ToggleBox>

        </Col>

      </FormGroup><span style={{ paddingTop: '10px' }}></span>

      <hr/>

      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
          <i class="material-icons">description</i>
          <ToggleBox3 title="Documents Panel">
            <UploadFiles />
          </ToggleBox3>
          </FormGroup>

        </Col>
      </FormGroup>

      </FormGroup>



    </div>
  )

}





