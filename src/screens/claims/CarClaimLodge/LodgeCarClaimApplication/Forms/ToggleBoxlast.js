import React, { Component } from "react";

class ToggleBoxlast extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			opened: false,
		};
		this.toggleBox = this.toggleBox.bind(this);
	}
  
	toggleBox() {
		const { opened } = this.state;
		this.setState({
			opened: !opened,
		});
	}
  
	render() {
		var { title, children } = this.props;
		const { opened } = this.state;

		if (opened){
			title =<span style={{color:"#4d79ff",fontSize:'15px'}}>Minimize Terms and Conditions Paragraph</span>;
		}else{
			title =<span style={{color:'#3262a8',fontSize:'15px'}}>Accept Terms And Conditions</span>;
		}

		return (
			<div className="box">
				<div className="boxTitle" onClick={this.toggleBox}>
					{title}
				</div>
				{opened && (					
					<div class="boxContent">
						{children}
					</div>
				)}
			</div>
		);
	}
}

export default ToggleBoxlast;

