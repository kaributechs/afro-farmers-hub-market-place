import React ,{Component}from 'react'
import { Link } from 'react-router-dom';
import ToggleBox2 from "./ToggleBox2";
import Recorder2 from './Recorder2';


class Audio extends Component {

    constructor() {
  
      super();
  
      this.state = {
  
        name: "Language"
  
      };
  
      this.onValueChange = this.onValueChange.bind(this);
  
      this.formSubmit = this.formSubmit.bind(this);
  
    }
  
  
    onValueChange(event) {
  
      this.setState({
  
        selectedOption: event.target.value
  
      });
  
    }
  
  
    formSubmit(event) {
  
      event.preventDefault();
  
      console.log(this.state.selectedOption)
  
    }
  
  
    render() {
  
      return (
  
        <form onSubmit={this.formSubmit}>
            <h4 style={{fontSize:17}}>Record Additional Audio</h4>
          
            <h4 style={{fontSize:17}}>Step 1:</h4>
            <span style={{color:'brown',fontSize:14}}>Select the language you are going to use:</span>
            <br/>
            <input
  
                type="radio"
  
                value="English"
  
                checked={this.state.selectedOption === "English"}
  
                onChange={this.onValueChange}
  
              />
              <span style={{paddingRight:'5px'}}></span>

              English
            <span style={{paddingRight:'25px'}}></span>
  
              <input
  
                type="radio"
  
                value="Shona"
  
                checked={this.state.selectedOption === "Shona"}
  
                onChange={this.onValueChange}
  
              />
              <span style={{paddingRight:'5px'}}></span>

              Shona
            <span style={{paddingRight:'25px'}}></span>

  
              <input
  
                type="radio"
  
                value="Xhosa"
  
                checked={this.state.selectedOption === "Xhosa"}
  
                onChange={this.onValueChange}
  
              />
              <span style={{paddingRight:'5px'}}></span>

              Xhosa

            <span style={{paddingRight:'25px'}}></span>

  
              <input
  
                type="radio"
  
                value="Zulu"
  
                checked={this.state.selectedOption === "Zulu"}
  
                onChange={this.onValueChange}
  
              />
              <span style={{paddingRight:'5px'}}></span>

              Zulu  
          
              <span style={{paddingRight:'25px'}}></span><br/>
  

          <div>
          <br/>
          <h4 style={{fontSize:17}}>Step 2:</h4>

          </div>

          <ToggleBox2 title="Recording panel">
            <Recorder2 />
          </ToggleBox2>

  
        </form>
  
      );
  
    }
  
  }
  
  
  export default Audio;
