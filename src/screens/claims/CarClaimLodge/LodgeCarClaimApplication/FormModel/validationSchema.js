import * as Yup from 'yup';
import lodgeCarClaimModel from './lodgeCarClaimModel'

const {
  formField: {
    InsuredDetails:{
    insuredId,
    insuredSalutation,
    insuredFirstName,
    insuredLastName,
    insuredPhoneNumber,
    insuredEmail,
    insuredResidentialStreetAddress,
    insuredSurbub,
    insuredResidentialCity,
    insuredRegion,
    insuredCountry,
    insuredZipCode,
    insuredWorkingStreetAddress,
    insuredWorkingSurbub,
    insuredWorkingCity,
    insuredWorkingRegion,
    insuredWorkingCountry,
    insuredWorkingZipCode
    },
    
    VehicleDetails:{
    vehicleMake,
    vehicleModel,
    vehicleModelYear,
    vehicleRegistrationNumber,
    vehicleMilage,
    vehicleUsage
    },
    
    DriversDetails:{
      driversFirstName,
      driversLastName,
      driversEmail,
      driversPhoneNumber,
      driversResidentialStreetAddress,
      driversSurbub,
      driversResidentialCity,
      driversCountry,
      driversRegion,
      driversZipCode,
      driversWorkingStreetAddress,
      driversWorkingSurbub,
      driversWorkingCity,
      driversWorkingRegion,
      driversWorkingCountry,
      driversWorkingZipCode,
      driversLicenseNumber,
      dateOfLicenseIssue
    },
   
    FinalPageDetails:{
    accidentHistory,
    driversCondition,
    accidentHistoryDetails,
    driversConditionDetails,
    vehiclePurpose,
    natureOfInjuries,
    accidentDescription

    }
    
  }
} = lodgeCarClaimModel;

export default[
  

      ];

   
        
