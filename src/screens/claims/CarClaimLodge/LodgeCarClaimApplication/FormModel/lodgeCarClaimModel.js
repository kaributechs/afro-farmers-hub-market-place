
export default {

formId: 'lodgeCarClaimModel',

formField: {
    screenId:{
        name:"screenId"
    },
InsuredDetails:{ 
insuredId:{
    name: "insuredId"
},
insuredSalutation:{
    name: "insuredSalutation"
},
insuredFirstName:{
    name: "insuredFirstName"
},
insuredLastName: {
    name: "insuredLastName"
},
insuredPhoneNumber:{
    name:"insuredPhoneNumber"
},
insuredEmail:{
    name:"insuredEmail"
},
insuredResidentialStreetAddress:{
    name:"insuredResidentialStreetAddress"
},
insuredSurbub:{
    name:"insuredSurbub"
},
insuredResidentialCity:{
    name:"insuredResidentialCity"
},
insuredRegion:{
    name:"insuredRegion"
},
insuredCountry:{
    name:"insuredCountry"
},
insuredZipCode:{
    name:"insuredZipCode"
},
insuredWorkingStreetAddress:{
    name:"insuredWorkingStreetAddress"
},
insuredWorkingSurbub:{
    name:"insuredWorkingSurbub"
},
insuredWorkingCity:{
    name:"insuredWorkingCity"
},
insuredWorkingRegion:{
    name:"insuredWorkingRegion"
},
insuredWorkingCountry:{
    name:"insuredWorkingCountry"
},
insuredWorkingZipCode:{
    name:"insuredWorkingZipCode"
}
},

VehicleDetails:{
vehicleMake:{
    name:"vehicleMake"
},
vehicleModel:{
    name:"vehicleModel"
},
vehicleModelYear:{
    name:"vehicleModelYear"
},
vehicleRegistrationNumber:{
    name:"vehicleRegistrationNumber"
},
vehicleUsage:{
    name:"vehicleUsage"
},
vehicleMilage:{
    name:"vehicleMilage"
}
},

DriversDetails:{


driversFirstName:{
    name:"driversFirstName"
},
driversLastName:{
    name:"driversLastName"
},
driversPhoneNumber:{
    name:"driversPhoneNumber"
},
driversEmail:{
    name:"driversEmail"
},
driversResidentialStreetAddress:{
    name:"driversResidentialStreetAddress"
},
driversSurbub:{
    name:"driversSurbub"
},
driversResidentialCity:{
    name:"driversResidentialCity"
},
driversRegion:{
    name:"driversRegion"
},
driversCountry:{
    name:"driversCountry"
},
driversZipCode:{
    name:"driversZipCode"
},
driversWorkingStreetAddress:{
    name:"driversWorkingStreetAddress"
},
driversWorkingSurbub:{
    name:"driversWorkingSurbub"
},
driversWorkingCity:{
    name:"driversWorkingCity"
},
driversWorkingRegion:{
    name:"driversWorkingRegion"
},
driversWorkingCountry:{
    name:"driversWorkingCountry"
},
driversWorkingZipCode:{
    name:"driversWorkingZipCode"
},
driversLicenseNumber:{
    name:"driversLicenseNumber"
},
dateOfLicenseIssue:{
    name:"dateOfLicenseIssue"
}
},

FinalPageDetails:{
accidentHistory:{
    name:"accidentHistory"
},
driversCondition:{
    name:"driversCondition"
},
accidentHistoryDetails:{
    name:"accidentHistoryDetails"
},
driversConditionDetails:{
    name:"driversConditionDetails"
},
vehiclePurpose:{
    name:"vehiclePurpose"
},
natureOfInjuries:{
    name:"natureOfInjuries"
},
accidentDescription:{
    name:"accidentDescription"
}

}


}

};