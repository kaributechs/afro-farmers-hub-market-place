import React, { useState } from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { useFormikContext } from 'formik';
import { CFormLabel } from "@coreui/react";
import { Link } from 'react-router-dom'
import ToggleBoxlast from './Forms/ToggleBoxlast';
import TermsAndConditions from './Forms/TermsAndConditions';



const SubmitForm = (props) => {
    const {
        formField: {
          screenId,
        InsuredDetails:{
         insuredId,
         insuredSalutation,
         insuredFirstName,
         insuredLastName,
         insuredPhoneNumber,
         insuredEmail,
         insuredResidentialStreetAddress,
         insuredResidentialCity,
         insuredRegion,
         insuredCountry,
         insuredZipCode,
         insuredWorkingStreetAddress,
         insuredWorkingCity,
         insuredWorkingRegion,
         insuredWorkingCountry,
         insuredWorkingZipCode}, 
      
        VehicleDetails:{
         vehicleMake,
         vehicleModel,
         vehicleModelYear,
         vehicleRegistrationNumber,
         vehicleMilage,
         vehicleUsage
          },
          
      
        DriversDetails:{
          driversFirstName,
          driversLastName,
          driversEmail,
          driversPhoneNumber,
          driversResidentialStreetAddress,
          driversResidentialCity,
          driversRegion,
          driversCountry,
          driversZipCode,
          driversWorkingStreetAddress,
          driversWorkingCity,
          driversWorkingRegion,
          driversWorkingCountry,
          driversWorkingZipCode,
          driversLicenseNumber,
          dateOfLicenseIssue
          },
      
          FinalPageDetails:{
          driversCondition,
          accidentHistory,
          accidentHistoryDetails,
          driversConditionDetails,
          vehiclePurpose, 
          natureOfInjuries,
          accidentDescription,
      
          },
        }
        
      }= props;
      screenId.name = 'Submit_Form_Page'

  return (

    <div className="animated fadeIn">
       
        <Col xs="12" sm="6" lg="6">    
           <strong>Declaration</strong> 
          
        </Col>
        <hr/>
        <Col >  
        <p style={{fontSize:'15px',fontWeight:'bold'}}>
        I hereby declare that the details filled are true and correct to the best of my belief and knowledge.In the event that the information 
        I have given or any part thereof is found incorrect, I agree that all right under the policy will be forfeited. 
        </p>
        </Col>
        <ToggleBoxlast>
            <TermsAndConditions/>
        </ToggleBoxlast>
  

    </div>
  )

}
export default SubmitForm






