import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import lodgeCarClaimModel from "./FormModel/lodgeCarClaimModel";
import formInitValues from './FormModel/formInitValues';
import validationSchema from "./FormModel/validationSchema";
import Stepper from "react-stepper-horizontal";
import { Button, Card, CardBody, CardHeader } from "reactstrap";
import { InsuredForm, VehicleForm, DriversForm, QuestionsForm } from "./Forms";
import ReviewForm from './ReviewForm'
import SubmitForm from './SubmitForm'
import { CButton, CCardFooter } from "@coreui/react";
import { Link, StaticRouter } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { insuredFormTask, vehicleFormTask, driversFormTask, questionsFormTask } from 'src/store/actions';
import { onSuccess,onError } from 'src/utils';
import axios from 'axios';
const { formId, formField } = lodgeCarClaimModel;


function _renderStepContent(step) {
  switch (step) {
    case 0:
      return <InsuredForm formField={formField} />;
    case 1:
      return <VehicleForm formField={formField} />;
    case 2:
      return <DriversForm formField={formField} />;
    case 3:
      return <QuestionsForm formField={formField} />;
    case 4:
      return <ReviewForm formField={formField} />;
    case 5:
      return <SubmitForm formField={formField} />;

    default:
      return <InsuredForm formField={formField} />;
  }

}

export const LodgeCarClaimFormStepper = () => {

  const dispatch = useDispatch();
  const insuredObj = useSelector(state => state.insured?.formdata);


  const [activeStep, setActiveStep] = useState(0);
  const sections = [
    { title: "Insured's Details", onClick: () => setActiveStep(0) },
    { title: 'Vehicle Details', onClick: () => setActiveStep(1) },
    { title: "Driver's Details", onClick: () => setActiveStep(2) },
    { title: "Insurance Questions", onClick: () => setActiveStep(3) },
    { title: "Form Review", onClick: () => setActiveStep(4) },
    { title: "Submit Form", onClick: () => setActiveStep(5) },



  ];

  const currentValidationSchema = validationSchema[activeStep];
  const isLastStep = activeStep === sections.length - 1;



  async function _submitForm(values, actions) {

    alert(JSON.stringify(values, null, 2));
    console.log(values)
    setTimeout(() => {
      axios
        .post(`/api/v1/save`, values)
        .then((data) => {
          onSuccess()
          console.log(data.data);
        })
        .catch((error) => {
          console.log(error);
          onError()
        });
        actions.setSubmitting(false)
  
    }, 2000)
    
  }

  function _handleSubmit(values, actions) {

    if (activeStep === 0) {
      dispatch(insuredFormTask(values))

    }

    if (activeStep === 1) {
      dispatch(vehicleFormTask(values))
    }

    if (activeStep === 2) {
      dispatch(driversFormTask(values))
    }
    if (activeStep === 3) {
      dispatch(questionsFormTask(values))

    }

    if (isLastStep) {
      _submitForm(values, actions);
      dispatch(driversFormTask(values))
    } else {
      setActiveStep(activeStep + 1);
      actions.setTouched({});
      actions.setSubmitting(false);
    }
  }

  const prev = () => {
    setActiveStep(activeStep - 1);
  }

  function save(values, actions) {
    if (activeStep === 0) {
      dispatch(insuredFormTask(values))

    }

    if (activeStep === 1) {
      dispatch(vehicleFormTask(values))
    }

    if (activeStep === 2) {
      dispatch(driversFormTask(values))
    }

    if (activeStep === 3) {
      dispatch(questionsFormTask(values))
    }
    else {
     //
    }

  }

  return (

    <div classname='container'>

      <React.Fragment>
        <Link to="/dashboard" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
        <Card>
          <CardHeader>
            <h4>Lodge Claim </h4>
            <Stepper
              steps={sections}
              activeStep={activeStep}
              activeColor="grey"
              defaultBarColor="grey"
              completeColor="#4dbd74"
              completeBarColor="grey"
            />

            <br />

          </CardHeader>

          {activeStep === sections.length ? (
            <InsuredForm />
          ) : (
            <Formik
              initialValues={formInitValues}
              validationSchema={currentValidationSchema}
              onSubmit={_handleSubmit}

            >
              {({
                isSubmitting
              }) => (
                <Form id={formId}>
                  <CardBody>
                    {_renderStepContent(activeStep)}
                  </CardBody>
                  <CCardFooter>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>

                      {activeStep !== 0 ? (
                        <CButton className=" mb-3" onClick={prev}>Back</CButton>
                      ) : <span></span>}

                      <CButton className=" mb-3" onClick={save}>Save</CButton>



                      <CButton className=" mb-3" type="submit">
                        {isLastStep ? 'Submit' : 'Next'}
                      </CButton>

                    </div>
                  </CCardFooter>
                </Form>
              )}
            </Formik>

          )}
        </Card>
      </React.Fragment>
    </div>

  );

}





