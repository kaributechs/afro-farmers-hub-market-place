import React from 'react';
import {useField} from 'formik';
import { CFormLabel} from "@coreui/react";
import {FormFeedback, FormGroup} from "reactstrap";
import { RegionDropdown} from "react-country-region-selector";

export const InputRegionDropdownField = (props) => {
  const {errorText, name, label, ...rest} = props;
  const [field, meta, helper] = useField(props);
  const {setValue} = helper;

  const _onChange =(event)=>{
    debugger;
    setValue(event);
  }
  return (

    <FormGroup>
      <CFormLabel htmlFor={name}>{label}</CFormLabel>
      <RegionDropdown
        className=" form-control mb-3"
        onChange={_onChange}
        {...field}
        {...rest}/>
      <FormFeedback>{meta.error}</FormFeedback>
    </FormGroup>

  );
}
