import React, { useState } from "react";
import axios from "axios";

import { Link } from 'react-router-dom'

function SubmitDocumentation() {
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const handleSubmission = () => {
    
    const formData = new FormData();

    formData.append("files", selectedFile);


    const uploadFile = () =>{
        axios.post(`/api/v1/files/upload`,  
            formData
        ).then(()=>{
          console.log("**Sucesss*")
        }).catch(()=>{
          console.log("***Errors**")
        })
      

    }

    uploadFile();

  };

  return (
    <div>
    <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <input type="file" name="file" onChange={changeHandler} />
      {isFilePicked ? (
        <div>
          <p>Filename: {selectedFile.name}</p>
          <p>Filetype: {selectedFile.type}</p>
          <p>Size in bytes: {selectedFile.size}</p>
          <p>
            lastModifiedDate:{" "}
            {selectedFile.lastModifiedDate.toLocaleDateString()}
          </p>
        </div>
      ) : (
        <p>Select a file to show details</p>
      )}
      <div>
        <button className="btn btn-success" onClick={handleSubmission}>Submit</button>
      </div>
    </div>
  );
}

export default SubmitDocumentation;



