import React from 'react';
import {  Button,Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    vehicleRegistrationNumber: Yup.string()
    .required('Vehicle Registration Number is Required!'),
    vehicleMake: Yup.string()
    .required('Vehicle Make is Required!'),
    vehicleModel: Yup.string()
    .required('Vehicle Model is Required!'),
    vehicleYear: Yup.string()
    .max(Date(Date.now()), "Vehicle Year Invalid")
    .required('Vehicle Year is Required!'),
    nameOfHirePurchaseCompany: Yup.string()
    .required('This field is Required!'),
    amountOutstanding: Yup.string()
    .required('Date Is Required'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
 
  vehicleRegistrationNumber:"",
  vehicleMake:"",
  vehicleModel:"",
  vehicleYear:"",
  nameOfHirePurchaseCompany:"",
  amountOutstanding:"",
  

}

const onSubmit = (values, { setSubmitting }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const VehicleDetails = () =>{


  /*

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        vehicleRegistrationNumber:true,
        clientLicenseNumber:true,
        vehicleMake:true,
        vehicleModel:true,
        vehicleYear:true,
        vehicleMileage:true,
        vehicleWorth:true,
        vehicleUsage:true,
        numberOfSeats:true,
        vehicleHasAlarmSystem:true,
        residentHasNightParking:true,
        amountOutstanding:true,
        dateWhenLicenseWasObtained:true,
      
      }
    )
    validateForm(errors)
  }*/
  return(
  
    <div className="animated fadeIn">
    <Link to="/claims/type/car" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Lodge Claim</strong>
        </CardHeader>
        <CardBody>
          <strong>Vehicle Details</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,

              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="vehicleRegistrationNumber"><span style={{color:"red"}}>*</span>Vehicle Registration Number</Label>
                            <Input type="text"
                                  name="vehicleRegistrationNumber"
                                  id="vehicleRegistrationNumber"
                                  placeholder="Vehicle Registration Number"
                                  autoComplete="vehicle registration number"
                                  valid={!errors.vehicleRegistrationNumber}
                                  invalid={touched.vehicleRegistrationNumber && !!errors.vehicleRegistrationNumber}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.vehicleRegistrationNumber} />
                            <FormFeedback>{errors.vehicleRegistrationNumber}</FormFeedback>
                          </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="vehicleMake"><span style={{color:"red"}}>*</span>Vehicle Make</Label>
                        <Input type="select" name="vehicleMake" id="vehicleMake" 
                        valid={!errors.vehicleMake}
                        invalid={touched.vehicleMake && !!errors.vehicleMake}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.vehicleMake} >
                          <option value="" disabled selected hidden>Select your Vehicle Make</option>   
                          <option value="ACURA">ACURA</option>
                          <option value="ASTON MARTIN">ASTON MARTIN</option>
                          <option value="AUDI">AUDI</option>
                          <option value="BENTLEY">BENTLEY</option>
                          <option value="BMW">BMW</option>
                          <option value="BUICK">BUICK</option>
                          <option value="CADILLAC">CADILLAC</option>
                          <option value="CHEVROLET">CHEVROLET</option>
                          <option value="CHRYSLER">CHRYSLER</option>
                          <option value="DODGE">DODGE</option>
                          <option value="FERRARI">FERRARI</option>
                          <option value="FORD">FORD</option>
                          <option value="GMC">GMC</option>
                          <option value="HONDA">HONDA</option>
                          <option value="HUMMER">HUMMER</option>
                          <option value="HYUNDAI">HYUNDAI</option>
                          <option value="INFINITI">INFINITI</option>
                          <option value="ISUZU">ISUZU</option>
                          <option value="JAGUAR">JAGUAR</option>
                          <option value="JEEP">JEEP</option>
                          <option value="KIA">KIA</option>
                          <option value="LAMBORGHINI">LAMBORGHINI</option>
                          <option value="LAND ROVER">LAND ROVER</option>
                          <option value="LEXUS">LEXUS</option>
                          <option value="LINCOLN">LINCOLN</option>
                          <option value="LOTUS">LOTUS</option>
                          <option value="MASERATI">MASERATI</option>
                          <option value="MAYBACH">MAYBACH</option>
                          <option value="MAZDA">MAZDA</option>
                          <option value="MERCEDES-BENZ">MERCEDES-BENZ</option>
                          <option value="MERCURY">MERCURY</option>
                          <option value="MINI">MINI</option>
                          <option value="MITSUBISHI">MITSUBISHI</option>
                          <option value="NISSAN">NISSAN</option>
                          <option value="PONTIAC">PONTIAC</option>
                          <option value="PORSCHE">PORSCHE</option>
                          <option value="ROLLS-ROYCE">ROLLS-ROYCE</option>
                          <option value="SAAB">SAAB</option>
                          <option value="SATURN">SATURN</option>
                          <option value="SUBARU">SUBARU</option>
                          <option value="SUZUKI">SUZUKI</option>
                          <option value="TOYOTA">TOYOTA</option>
                          <option value="VOLKSWAGEN">VOLKSWAGEN</option>
                          <option value="VOLVO">VOLVO</option>
                          <option value="other">Other</option>
                        </Input>
                        <FormFeedback>{errors.vehicleMake}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="vehicleModel"><span style={{color:"red"}}>*</span>Vehicle Model</Label>
                      <Input type="text"
                            name="vehicleModel"
                            id="vehicleModel"
                            placeholder="Vehicle Model"
                            autoComplete="vehicle-model"
                            valid={!errors.vehicleModel}
                            invalid={touched.vehicleModel && !!errors.vehicleModel}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.vehicleModel} />
                      <FormFeedback>{errors.vehicleModel}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="vehicleYear"><span style={{color:"red"}}>*</span>Vehicle Year</Label>
                            <Input type="date"
                                  name="vehicleYear"
                                  id="vehicleYear"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-vehicleYear"
                                  valid={!errors.vehicleYear}
                                  invalid={touched.vehicleYear && !!errors.vehicleYear}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.vehicleYear} />
                            <FormFeedback>{errors.vehicleYear}</FormFeedback>
                          </FormGroup>
                        </Col>
                    </FormGroup>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="residentHasNightParking">Name of hire-purchase company if any</Label>
                        <Input type="text" name="hire-purchaseco." id="nameOfHirePurchaseCompany" placeholder="name of company"
                        valid={!errors.nameOfHirePurchaseCompany}
                        invalid={touched.nameOfHirePurchaseCompany && !!errors.nameOfHirePurchaseCompany}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.nameOfHirePurchaseCompany}/>
                        
                        <FormFeedback>{errors.nameOfHirePurchaseCompany}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="amountOutstanding">Amount Oustanding</Label>
                        <Input type="text" name="amountOutstanding" id="amountOutstanding" placeholder="Enter outstanding amount if car was hired or owing "
                        valid={!errors.amountOutstanding}
                        invalid={touched.amountOutstanding && !!errors.amountOutstanding}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.amountOutstanding} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.amountOutstanding}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>  
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6"> 
                    <FormGroup>
              <Link to="/claims/type/car">
                      <Button type="submit" color="success" className="mr-1" >Back</Button></Link>
                    </FormGroup> 
                    </Col>

                    <Col  xs="12" sm="6"  lg="6"> 
                    <FormGroup>
              <Link to="/claims/type/car3">
                      <Button type="submit" color="success" className="mr-1" >Next</Button></Link>
                    </FormGroup> 
                    </Col>

                    </FormGroup>             
                    </Form>
                    
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default VehicleDetails;