import React from "react";
import { Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import ScreensCards from "../../../components/cards/ScreensCards";
import useTrans from "../../../hooks/useTrans";

const DamageScreen = () => {

    // eslint-disable-next-line
  const [t, handleClick] = useTrans();

  return (
    <div>
      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("HowItWorks.1")}
            footerText={t("HowItWorks.1")}
            cardRoute="howitworks"
          />
        </Col>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={t("AssessDamage.1")}
            footerText={t("Assess.1")}
            cardRoute="acessdamage"
          />
        </Col>
      </Row>
    </div>
  );
};

export default DamageScreen;
