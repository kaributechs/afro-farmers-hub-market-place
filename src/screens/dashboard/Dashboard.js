import React from "react";
import { Col, Row } from "reactstrap";
import DashBoardCards from "../../components/cards/DashBoardCards";
import useTrans from "../../hooks/useTrans";

const Dashboard = () => {
  // eslint-disable-next-line
  const [t, handleClick] = useTrans();

  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Suppliers")}
            footerText={t("View Suppliers")}
            cardRoute = "suppliers"
          />
        </Col>

        <Col xs="12" sm="6" lg="3">
          <DashBoardCards
            cardTitle={t("Products")}
            footerText={t("View Products")}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;
