import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup, Label} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  DriverDetailsForm = (props)=> {
  const {
    formField: {
    VehicleUserDetails: {
    vehicleUserDetails_LastName,
    vehicleUserDetails_FirstName,
    vehicleUserDetails_LicenseNumber,
    vehicleUserDetails_NationalId,
    vehicleUserDetails_DateOfBirth,
    vehicleUserDetails_EmailAddress,
    vehicleUserDetails_PhoneNumber,
    vehicleUserDetails_Gender,
    vehicleUserDetails_Role,
    vehicleUserDetails_RoleDescription,
    vehicleUserDetails_MaritalStatus,
    }
    }
  } = props;

  const maritalStatuses = [
    {text: "Select your Marital Status", value:""},
      {text: "Single", value: "single"},
      {text: "Married", value: "married"},
      {text: "Widowed", value: "widowed"},
      {text: "Partnership", value: "partnership"},
      {text: "Other", value: "other"},
  ]

  const genders = [
    {text: "Select Gender", value:""},
      {text: "Female", value: "female"},
      {text: "Male", value: "male"},
  ]


   return(

    <div className="animated fadeIn">

          <strong>Vehicle User Details</strong>
          <hr />


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="vehicleUserDetails_NationalId"><span style={{color:"red", size:12}}>*</span>National Id/ Passport Number</Label>}
                              name="vehicleUserDetails_NationalId"
                              id="vehicleUserDetails_NationalId"
                              placeholder="National Id/ Passport Number"
                              autoComplete="national-id"/>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="vehicleUserDetails_LicenseNumber"><span style={{color:"red", size:12}}>*</span>License Number</Label>}
                              name="vehicleUserDetails_LicenseNumber"
                              id="vehicleUserDetails_LicenseNumber"
                              placeholder="License Number"/>
                      </Col>
                      </FormGroup>

 <div><br /></div>

                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                          <InputField type="text"
                                label = {<Label htmlFor="vehicleUserDetails_FirstName"><span style={{color:"red", size:12}}>*</span>First Name</Label>}
                                name="vehicleUserDetails_FirstName"
                                id="vehicleUserDetails_FirstName"
                                placeholder="First Name"
                                autoComplete="first-name"/>
                        </Col>

                      <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="vehicleUserDetails_LastName"><span style={{color:"red", size:12}}>*</span>Last Name</Label>}
                              name="vehicleUserDetails_LastName"
                              id="vehicleUserDetails_LastName"
                              placeholder="Last Name"
                              autoComplete="Last name"/>
                      </Col>
                    </FormGroup>

 <div><br /></div>

                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <InputField type="vehicleUserDetails_EmailAddress"
                              label = {<Label htmlFor="vehicleUserDetails_EmailAddress"><span style={{color:"red", size:12}}>*</span>Email</Label>}
                              name="vehicleUserDetails_EmailAddress"
                              id="vehicleUserDetails_EmailAddresss"
                              placeholder="Email"
                              autoComplete="email"/>
                   </FormGroup>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                            <InputField type="text"
                              label = {<Label htmlFor="vehicleUserDetails_PhoneNumber"><span style={{color:"red", size:12}}>*</span>Phone Number</Label>}
                                  name="vehicleUserDetails_PhoneNumber"
                                  id="vehicleUserDetails_PhoneNumber"
                                  placeholder="Phone Number"/>
                        </Col>
                    </FormGroup>

 <div><br /></div>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                            <InputField type="date"
                              label = {<Label htmlFor="dependentDateOfBirth"><span style={{color:"red", size:12}}>*</span>Date Of Birth</Label>}
                                  name="vehicleUserDetails_DateOfBirth"
                                  id="dependentDateOfBirth"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-dateOfBirth"/>
                        </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={genders}
                                  name={vehicleUserDetails_Gender.name}
                                  id={vehicleUserDetails_Gender.name}
                                  label = {<Label htmlFor="vehicleUserDetails_Gender"><span style={{color:"red", size:12}}>*</span>Gender</Label>}
                                  value="value" text="text"/>
                    </Col>
                    </FormGroup>

 <div><br /></div>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={maritalStatuses}
                                  name={vehicleUserDetails_MaritalStatus.name}
                                  id={vehicleUserDetails_MaritalStatus.name}
                                  label = {<Label htmlFor="vehicleUserDetails_MaritalStatu"><span style={{color:"red", size:12}}>*</span>Marital Status</Label>}
                                  value="value" text="text"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label="Role of Applicant at the Company"
                               name={vehicleUserDetails_Role.name}
                               id={vehicleUserDetails_Role.name}
                               placeholder="Role"/>

                    </Col>
        </FormGroup>

 <div><br /></div>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               rows={2}
                               label="Role Description"
                               name={vehicleUserDetails_RoleDescription.name}
                               id={vehicleUserDetails_RoleDescription.name}
                               placeholder="Role Description"/>
                    </Col>

        </FormGroup>



    </div>
  )
}
