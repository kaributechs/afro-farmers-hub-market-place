import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup, Label} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  InsuranceQuestions = (props)=> {
  const {
    formField: {
    InsuranceQuestions: {
  insuranceQuestions_carInsuranceType,
  insuranceQuestions_thirdPartyLiability,
  insuranceQuestions_allRisksYesOrNo,
  insuranceQuestions_replacementCarYesOrNo,
  insuranceQuestions_periodOfInsuranceFrom,
  insuranceQuestions_periodOfInsuranceTo,
 }
    }
  } = props;

  const insuranceTypes = [
    {text: "Select your Insurance Type", value:""},
      {text: "Collision Coverage", value: "Collision Coverage"},
      {text: "Liability Coverage", value: "Liability Coverage"},
      {text: "Comprehensive Coverage", value: "Comprehensive Coverage"},
      {text: "Personal Injury Protection", value: "Personal Injury Protection"},
  ]

  const thirdParty = [
    {text: "Third Party Liability", value: ""},
    {text: "Yes", value: "Yes"},
    {text: "No", value: "No"},

  ]

  const yesNo = [
    {text: "Select", value:""},
    {text: "Yes", value: "Yes"},
    {text: "No", value: "No"},

  ]

   return(

    <div className="animated fadeIn">

          <strong>Insurance Questions</strong>
          <hr />


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={insuranceTypes}
                                  name={insuranceQuestions_carInsuranceType.name}
                                  id={insuranceQuestions_carInsuranceType.name}
                                  label = {<Label htmlFor="insuranceQuestions_carInsuranceType"><span style={{color:"red", size:12}}>*</span>Type of Car Insurance</Label>}
                                  value="value" text="text"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={thirdParty}
                                  name={insuranceQuestions_thirdPartyLiability.name}
                                  id={insuranceQuestions_thirdPartyLiability.name}
                                  label = {<Label htmlFor="insuranceQuestions_thirdPartyLiability"><span style={{color:"red", size:12}}>*</span>Third Party Liability</Label>}
                                  value="value" text="text"/>
                    </Col>
                      </FormGroup>


                    <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={yesNo}
                                  name={insuranceQuestions_allRisksYesOrNo.name}
                                  id={insuranceQuestions_allRisksYesOrNo.name}
                                  label = {<Label htmlFor="insuranceQuestions_carInsuranceType"><span style={{color:"red", size:12}}>*</span>Do You Want the Insurance to Cover For all the Risks?</Label>}
                                  value="value" text="text"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={yesNo}
                                  name={insuranceQuestions_replacementCarYesOrNo.name}
                                  id={insuranceQuestions_replacementCarYesOrNo.name}
                                  label = {<Label htmlFor="insuranceQuestions_replacementCarYesOrNo"><span style={{color:"red", size:12}}>*</span>Would You Want A Courtesy Car?</Label>}
                                  value="value" text="text"/>
                    </Col>
                   </FormGroup>


                    <FormGroup  row className="my-0">
                    <Col xs="12" sm="6" lg="6">
                      <FormGroup>
                        <strong>Period of Insurance</strong>

                      </FormGroup>
                    </Col>

                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                            <InputField type="date"
                                  label = {<Label htmlFor="insuranceQuestions_periodOfInsuranceFrom"><span style={{color:"red", size:12}}>*</span>From</Label>}
                                  name={insuranceQuestions_periodOfInsuranceFrom.name}
                                  id={insuranceQuestions_periodOfInsuranceFrom.name}
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="date"/>
                        </Col>

                    <Col  xs="12" sm="6"  lg="6">
                            <InputField type="date"
                                  label = {<Label htmlFor="insuranceQuestions_periodOfInsuranceTo"><span style={{color:"red", size:12}}>*</span>To</Label>}
                                  name={insuranceQuestions_periodOfInsuranceTo.name}
                                  id={insuranceQuestions_periodOfInsuranceTo.name}
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="date"/>
                        </Col>
                    </FormGroup>


    </div>
  )
}
