import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  InsuranceQuestionsForm = (props)=> {
  const {
    formField: {
      InsuranceQuestions:{
        carInsuranceType,
        thirdPartyLiability,
        bearsAllRisks,
        interestedInCourtesyCar,
        coversTotalLoss,
        coversExcessCosts,
        periodOfInsuranceFrom,
        periodOfInsuranceTo,
      }
    }
  } = props;

  const insuranceTypes = [
    {text: "Select your Insurance Type", value:""},
      {text: "Collision Coverage", value: "Collision Coverage"},
      {text: "Liability Coverage", value: "Liability Coverage"},
      {text: "Comprehensive Coverage", value: "Comprehensive Coverage"},
      {text: "Personal Injury Protection", value: "Personal Injury Protection"},
  ]

  const thirdParty = [
    {text: "Select", value: ""},
    {text: "Yes", value: "Yes"},
    {text: "No", value: "No"},

  ]

  const totalLoss = [
    {text: "Select", value: ""},
    {text: "Yes", value: "Yes"},
    {text: "No", value: "No"},

  ]

  const excessCosts = [
    {text: "Select", value: ""},
    {text: "R0 - R5000", value: "first"},
    {text: "R5000 - R15000", value: "second"},
    {text: "R15000 - R30000", value: "Third"},
    {text: "R30000 - R50000", value: "forth"},

  ]
  

  const isInterestedInCourtesyCar = [
    {text: "Select", value:""},
    {text: "Yes", value: "Yes"},
    {text: "No", value: "No"},

  ]

   return(

    <div className="animated fadeIn">

        <strong>Insurance Questions</strong>
        <hr />


        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={insuranceTypes}
                    name={carInsuranceType.name}
                    id={carInsuranceType.name}
                    label="Type of Car Insurance"
                    value="value" text="text"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={thirdParty}
                    name={thirdPartyLiability.name}
                    id={thirdPartyLiability.name}
                    label="Third Party Insurance"
                    value="value" text="text"/>
            </Col>
        </FormGroup>



        
        <FormGroup  row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={thirdParty}
                    name={bearsAllRisks.name}
                    id={bearsAllRisks.name}
                    label="Third Party Insurance"
                    value="value" text="text"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={isInterestedInCourtesyCar}
                    name={interestedInCourtesyCar.name}
                    id={interestedInCourtesyCar.name}
                    label="Would You Want A Courtesy Car?"
                    value="value" text="text"/>
            </Col>
        </FormGroup>

        <FormGroup  row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={totalLoss}
                    name={coversTotalLoss.name}
                    id={coversTotalLoss.name}
                    label="Would you like Total Loss Coverage?"
                    value="value" text="text"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={excessCosts}
                    name={coversExcessCosts.name}
                    id={coversExcessCosts.name}
                    label="How much Excess Costs would you prefer to cover?"
                    value="value" text="text"/>
            </Col>
        </FormGroup>


        <FormGroup  row className="my-0">
            <Col xs="12" sm="6" lg="6">
                <FormGroup>
                <strong>Period of Insurance</strong>

                </FormGroup>
            </Col>
        </FormGroup>


        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                    <InputField type="date"
                        label="From"
                        name={periodOfInsuranceFrom.name}
                        id={periodOfInsuranceFrom.name}
                        placeholder="yyyy/mm/dd"
                        autoComplete="new-dateOfBirth"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputField type="date"
                    label="To"
                    name={periodOfInsuranceTo.name}
                    id={periodOfInsuranceTo.name}
                    placeholder="yyyy/mm/dd"
                    autoComplete="new-dateOfBirth"/>
            </Col>
        </FormGroup>


    </div>
  )
}
