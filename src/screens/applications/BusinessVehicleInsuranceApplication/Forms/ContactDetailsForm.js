import React, {useState} from 'react';
import { Col,  FormGroup, Label} from 'reactstrap';
import {CountryDropdown, RegionDropdown} from 'react-country-region-selector';
import {useFormikContext} from 'formik';
import { InputField} from "../../FormFields";
import {CFormLabel} from "@coreui/react";

export const ContactDetailsForm = (props) => {

  const {
    formField: {
         ContactDetails: {
        contactDetails_clientEmailAddress,
        contactDetails_clientPhoneNumber,
        contactDetails_clientResidentialStreetAddress,
        contactDetails_clientResidentialCity,
        contactDetails_clientRegion,
        contactDetails_residentialZip,
        contactDetails_workAddressZip,
        contactDetails_clientCountry,
        contactDetails_clientWorkStreetAddress,
        contactDetails_clientWorkCity,
        contactDetails_workRegion,
        contactDetails_workCountry,
       }
    }

  } = props;

  //TODO: fix form field for country and region
  const {values: formValues} = useFormikContext();

  const [countryDll, setCountryDll] = useState(formValues.contactDetails_clientCountry )

  const [regionDll, setRegionDll] = useState(formValues.contactDetails_clientRegion )

  const [workCountryDll, setWorkCountryDll] = useState(formValues.contactDetails_workCountry )

  const [workRegionDll, setWorkRegionDll] = useState(formValues.contactDetails_workRegion)


  const handleCountryChange = (country) => {
    formValues.contactDetails_clientCountry = country;
    setCountryDll(country);
  }

  const handleRegionChange = (region) => {
    formValues.contactDetails_clientRegion = region;
    setRegionDll(region);
  }

  const handleWorkCountryChange = (contactDetails_workCountry) => {
    formValues.contactDetails_workCountry = contactDetails_workCountry;
    setWorkCountryDll(contactDetails_workCountry);
  }

  const handleWorkRegionChange = (contactDetails_workRegion) => {
    formValues.contactDetails_workRegion = contactDetails_workRegion;
    setWorkRegionDll(contactDetails_workRegion);
  }

  return (

    <div className="animated fadeIn">

      <strong>Contact Information</strong>
      <hr/>


      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="email"
                      label = {<Label htmlFor="contactDetails_clientEmailAddress"><span style={{color:"red", size:12}}>*</span>Email</Label>}
                      name={contactDetails_clientEmailAddress.name}
                      id={contactDetails_clientEmailAddress.name}
                      autoComplete="email"
                      placeholder="Email Address"/>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
                      label = {<Label htmlFor="contactDetails_clientPhoneNumber"><span style={{color:"red", size:12}}>*</span>Phone Number</Label>}
                       name={contactDetails_clientPhoneNumber.name}
                      id={contactDetails_clientPhoneNumber.name}
                      placeholder="Phone Number"/>

        </Col>
      </FormGroup>

      <hr/>

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Applicant Residential Address</strong>
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Company Address</strong>
          </FormGroup>
        </Col>
      </FormGroup>

<div><br /></div>

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
                      rows={2}
                      label = {<Label htmlFor="contactDetails_clientResidentialStreetAddress"><span style={{color:"red"}}>*</span>Street Address</Label>}
                      name={contactDetails_clientResidentialStreetAddress.name}
                      id={contactDetails_clientResidentialStreetAddress.name}
                      placeholder="Street Address"/>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="textarea"
                      rows={2}
                      label = {<Label htmlFor="contactDetails_clientWorkStreetAddress"><span style={{color:"red"}}>*</span>Work Street Address</Label>}
                      name={contactDetails_clientWorkStreetAddress.name}
                      id={contactDetails_clientWorkStreetAddress.name}
                      placeholder="Street Address"/>

        </Col>
      </FormGroup>

 <div><br /></div>

      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
                      label = {<Label htmlFor="contactDetails_clientResidentialCity"><span style={{color:"red", size:12}}>*</span>City</Label>}
                      name={contactDetails_clientResidentialCity.name}
                      id={contactDetails_clientResidentialCity.name}/>


        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="text"
                      label = {<Label htmlFor="contactDetails_clientWorkCity"><span style={{color:"red", size:12}}>*</span>City</Label>}
                      name={contactDetails_clientWorkCity.name}
                      id={contactDetails_clientWorkCity.name}/>

        </Col>
      </FormGroup>

<div><br /></div>
      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={contactDetails_clientCountry.name}><span style={{color:"red", size:12}}>*</span>Country</CFormLabel>
            <div style={{borderColor: 'green'}}>
              <CountryDropdown
                name={contactDetails_clientCountry.name}
                className=" form-control mb-3"
                id={contactDetails_clientCountry.name}
                value={countryDll}
                onChange={handleCountryChange}/>
            </div>
          </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={contactDetails_workCountry.name}><span style={{color:"red", size:12}}>*</span>Country</CFormLabel>
            <div style={{borderColor: 'green'}}>
              <CountryDropdown
                className=" form-control mb-3"
                name={contactDetails_workCountry.name}
                id={contactDetails_workCountry.name}
                value={workCountryDll}
                onChange={handleWorkCountryChange}/>
            </div>
          </FormGroup>


        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={formValues.contactDetails_clientRegion}><span style={{color:"red", size:12}}>*</span>Region</CFormLabel>
            <RegionDropdown
              country={formValues.contactDetails_clientCountry}
              className=" form-control mb-3"
              name={contactDetails_clientRegion.name}
              id={contactDetails_clientRegion.name}
              value={regionDll}
              onChange={handleRegionChange} />
          </FormGroup>



        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={contactDetails_workRegion.name}><span style={{color:"red", size:12}}>*</span>Region</CFormLabel>
            <RegionDropdown
              country={formValues.contactDetails_workCountry}
              className=" form-control mb-3"
              name={contactDetails_workRegion.name}
              id={contactDetails_workRegion.name}
              value={workRegionDll}
              onChange={handleWorkRegionChange} />
          </FormGroup>


        </Col>
      </FormGroup>
      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <InputField type="number"
                      label = {<Label htmlFor="contactDetails_residentialZip"><span style={{color:"red", size:12}}>*</span>Zip Code</Label>}
                      name={contactDetails_residentialZip.name}
                      id={contactDetails_residentialZip.name}/>


        </Col>

        <Col xs="12" sm="6" lg="6">
          <InputField type="number"
                      label = {<Label htmlFor="contactDetails_workAddressZip"><span style={{color:"red", size:12}}>*</span>Zip Code</Label>}
                      name={contactDetails_workAddressZip.name}
                      id={contactDetails_workAddressZip.name}/>

        </Col>
      </FormGroup>


    </div>
  )
}

