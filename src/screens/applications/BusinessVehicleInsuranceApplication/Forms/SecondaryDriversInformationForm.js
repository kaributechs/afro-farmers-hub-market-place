import React, {useState} from 'react';
import { Col,  FormGroup, Label} from 'reactstrap';
import {CountryDropdown, RegionDropdown} from 'react-country-region-selector';
import {useFormikContext} from 'formik';
import { InputField, InputSelect} from "../../FormFields";
import {CFormLabel} from "@coreui/react";

export const SecondaryDriversInformationForm = (props) => {
    
    const {
        formField: {
            SecondaryDriverDetails:{
                dependentLicenseNumber,
                dependentNationalId,
                relationshipToBeneficiary,
                dependentFirstName,
                dependentLastName,
                dependentEmailAddress,
                dependentDateOfBirth,
                dependentPhoneNumber,
                dependentGender,
                dependentDateWhenLicenseWasObtained
            }    
        }
    } = props;

    const genders = [      
        {text: "Select Your Gender", value:""},
          {text: "Male", value: "male"},
          {text: "Female", value: "female"},
          {text: "Other", value: "other"},
    ]

    const relationships =[
        {text: "Select Relationship", value:""},
        {text: "Husband", value: "husband"},
        {text: "Wife", value: "wife"},
        {text: "Friend", value: "friend"},
        {text: "Other", value: "other"},
    ]

    return (
        <div className="animated fadeIn">
            <strong>Secondary Drivers' Information</strong>
            <hr/>

            <FormGroup row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="text"
                        label="National Id/ Passport Number"
                        name={dependentNationalId.name}
                        id={dependentNationalId.name}
                        placeholder="National Id/ Passport Number"
                        autoComplete="national-id"/>
                </Col>

                <Col  xs="12" sm="6"  lg="6">
                    <InputSelect data={relationships} 
                        label="Relationship To Beneficiary"
                        name={relationshipToBeneficiary.name}
                        id={relationshipToBeneficiary.name}
                        value="value" text="text">
                    </InputSelect>
                </Col>
            </FormGroup>

              

              
            <FormGroup  row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="text"
                        label="First Name"
                        name={dependentFirstName.name}
                        id={dependentFirstName.name}
                        placeholder="First Name"
                        autoComplete="first-name"/>
                </Col>

                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="text"
                        label="Last Name"
                        name={dependentLastName.name}
                        id={dependentLastName.name}
                        placeholder="Last Name"
                        autoComplete="username"/>
              
                </Col>
            </FormGroup>




            <FormGroup  row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="dependentEmail"
                        label="Email"
                        name={dependentEmailAddress.name}
                        id={dependentEmailAddress.name}
                        placeholder="Email"
                        autoComplete="email" />
                </Col>

                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="text"
                        label="Phone Number"
                        name={dependentPhoneNumber.name}
                        id={dependentPhoneNumber.name}
                        placeholder="Phone Number"
                        autoComplete="phone-number"/>
                </Col>
            </FormGroup>
            



            <FormGroup row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="date"
                        label="Date of Birth"
                        name={dependentDateOfBirth.name}
                        id={dependentDateOfBirth.name}
                        placeholder="yyyy/mm/dd"
                        autoComplete="new-dateOfBirth"/>
                </Col>
                
                <Col  xs="12" sm="6"  lg="6">
                    <InputSelect data={genders}
                        label="Gender"
                        name={dependentGender.name}
                        id={dependentGender.name}
                        value="value" text="text">
                    </InputSelect>
                </Col>
            </FormGroup>


            <FormGroup row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="text"
                        label="License Number"
                        name={dependentLicenseNumber.name}
                        id={dependentLicenseNumber.name}
                        placeholder="License Number"
                        autoComplete="dependent-license-number"/>
                </Col>
                <Col  xs="12" sm="6"  lg="6">
                    <InputField type="date"
                        label="When Did You Obtain Your License?"
                        name={dependentDateWhenLicenseWasObtained.name}
                        id={dependentDateWhenLicenseWasObtained.name}
                        placeholder="yyyy/mm/dd"
                        autoComplete="dependentDateWhenLicenseWasObtained"/>
                </Col>
            </FormGroup>

        </div>
    )
}
