import React, {useState} from 'react';
import { Col,  FormGroup, Label} from 'reactstrap';
import {CountryDropdown, RegionDropdown} from 'react-country-region-selector';
import {useFormikContext} from 'formik';
import { InputField, InputSelect} from "../../FormFields";
import {CFormLabel} from "@coreui/react";


export const VehicleDetailsForm = (props) => {

    const {
        formField: {
            VehicleDetails:{
                vehicleRegistrationNumber,
                clientLicenseNumber,
                vehicleMake,
                vehicleModel,
                vehicleYear,
                vehicleMileage,
                vehicleWorth,
                numberOfSeats,
                vehicleHasAlarmSystem,
                residentHasNightParking,
                isVehicleUsedByOthers,
                dateWhenLicenseWasObtained
            }
        }
    } = props;

    const vehicleMakes = [
        {text: "Select Vehicle Make", value:""},
          {text: "Acura", value: "acura"},
          {text: "Aston Martin", value: "astonMartin"},
          {text: "Audi", value: "audi"},
          {text: "Bentley", value: "bentley"},
          {text: "BMW", value: "bmw"},
    ]

    const vehiclePurchasePrices =[
        {text: "Select from the range", value:""},
        {text: "0 - R100 000", value: "1"},
        {text: "R100 001 - R400 000", value: "2"},
        {text: "R400 000+", value: "3"},
    ]

    const isVehiclesUsedByOthers = [
        {text: "Select", value:""},
        {text: "Yes", value: "yes"},
        {text: "No", value: "no"},
    ]

    const isThereAnAlarmSystem = [
        {text: "Select", value:""},
        {text: "Yes", value: "yes"},
        {text: "No", value: "no"},
    ]

    const isThereNightParking= [
        {text: "Select", value:""},
        {text: "Yes", value: "yes"},
        {text: "No", value: "no"},
    ]

    {/*                    <option value="" disabled selected hidden>Select your Vehicle Make</option>   
                    <option value="BUICK">BUICK</option>
                    <option value="CADILLAC">CADILLAC</option>
                    <option value="CHEVROLET">CHEVROLET</option>
                    <option value="CHRYSLER">CHRYSLER</option>
                    <option value="DODGE">DODGE</option>
                    <option value="FERRARI">FERRARI</option>
                    <option value="FORD">FORD</option>
                    <option value="GMC">GMC</option>
                    <option value="HONDA">HONDA</option>
                    <option value="HUMMER">HUMMER</option>
                    <option value="HYUNDAI">HYUNDAI</option>
                    <option value="INFINITI">INFINITI</option>
                    <option value="ISUZU">ISUZU</option>
                    <option value="JAGUAR">JAGUAR</option>
                    <option value="JEEP">JEEP</option>
                    <option value="KIA">KIA</option>
                    <option value="LAMBORGHINI">LAMBORGHINI</option>
                    <option value="LAND ROVER">LAND ROVER</option>
                    <option value="LEXUS">LEXUS</option>
                    <option value="LINCOLN">LINCOLN</option>
                    <option value="LOTUS">LOTUS</option>
                    <option value="MASERATI">MASERATI</option>
                    <option value="MAYBACH">MAYBACH</option>
                    <option value="MAZDA">MAZDA</option>
                    <option value="MERCEDES-BENZ">MERCEDES-BENZ</option>
                    <option value="MERCURY">MERCURY</option>
                    <option value="MINI">MINI</option>
                    <option value="MITSUBISHI">MITSUBISHI</option>
                    <option value="NISSAN">NISSAN</option>
                    <option value="PONTIAC">PONTIAC</option>
                    <option value="PORSCHE">PORSCHE</option>
                    <option value="ROLLS-ROYCE">ROLLS-ROYCE</option>
                    <option value="SAAB">SAAB</option>
                    <option value="SATURN">SATURN</option>
                    <option value="SUBARU">SUBARU</option>
                    <option value="SUZUKI">SUZUKI</option>
                    <option value="TOYOTA">TOYOTA</option>
                    <option value="VOLKSWAGEN">VOLKSWAGEN</option>
                    <option value="VOLVO">VOLVO</option>
                    <option value="other">Other</option> */}

    return (
        <div className="animated fadeIn">

        <strong>Vehicle Details</strong>
        <hr />

        <FormGroup  row className="my-0">
            <Col  xs="12" sm="6"  lg="6">         
                <InputField type="text"
                    label = {<Label htmlFor="clientLicenseNumber"><span style={{color:"red", size:12}}>*</span>Client License Number</Label>}
                    name={clientLicenseNumber.name}
                    id={clientLicenseNumber.name}
                    placeholder="License Number"
                    autoComplete="license number"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputField type="date"
                    label = {<Label htmlFor="dateWhenLicenseWasObtained"><span style={{color:"red", size:12}}>*</span>When Did You Obtain Your License?</Label>}
                    name={dateWhenLicenseWasObtained.name}
                    id={dateWhenLicenseWasObtained.name}
                    placeholder="yyyy/mm/dd"
                    autoComplete="dateWhenLicenseWasObtained"/>
            </Col>
        </FormGroup>



        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputField type="text"
                    label = {<Label htmlFor="vehicleRegistrationNumber"><span style={{color:"red", size:12}}>*</span>Vehicle Registration Number</Label>}
                    name={vehicleRegistrationNumber.name}
                    id={vehicleRegistrationNumber.name}
                    autoComplete="Vehicle Registration Number"
                    placeholder="Vehicle Registration Number"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={vehicleMakes} 
                    label = {<Label htmlFor="vehicleMake"><span style={{color:"red", size:12}}>*</span>Vehicle Make</Label>}
                    name={vehicleMake.name} 
                    id={vehicleMake.name}
                    value="value" text="text">
                </InputSelect>
            </Col>
        </FormGroup>



        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputField type="text"
                    label = {<Label htmlFor="vehicleModel"><span style={{color:"red", size:12}}>*</span>Vehicle Model</Label>}
                    name={vehicleModel.name}
                    id={vehicleModel.name}
                    autoComplete="Vehicle Model"
                    placeholder="Vehicle Model"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputField type="date"
                    label = {<Label htmlFor="vehicleYear"><span style={{color:"red", size:12}}>*</span>Vehicle Year</Label>}
                    name={vehicleYear.name}
                    id={vehicleYear.name}
                    autoComplete="Vehicle Year"
                    placeholder="Vehicle Year"/>
            </Col>
        </FormGroup>




        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputField type="number"
                    label = {<Label htmlFor="vehicleMileage"><span style={{color:"red", size:12}}>*</span>Vehicle Mileage</Label>}
                    name={vehicleMileage.name}
                    id={vehicleMileage.name}
                    autoComplete="Vehicl Mileage"
                    placeholder="Vehicl Mileage"/>
            </Col>

            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={vehiclePurchasePrices}
                    label = {<Label htmlFor="vehiclePurchasePrice"><span style={{color:"red", size:12}}>*</span>Vehicle Purchase Price</Label>}
                    name={vehicleWorth.name}
                    id={vehicleWorth.name}
                    value="value" text="text">
                    
                </InputSelect>
            </Col>
        </FormGroup>


<<<<<<< HEAD
                  <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                            <InputField type="number"
                                  label = {<Label htmlFor="vehicleDetails_Mileage"><span style={{color:"red", size:12}}>*</span>Vehicle Mileage</Label>}
                                  name={vehicleDetails_Mileage.name}
                                  id={vehicleDetails_Mileage.name}
                                  placeholder="Vehicle Mileage"/>
                        </Col>
=======
>>>>>>> master



        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputField type="number"
                    label = {<Label htmlFor="numberOfSeats"><span style={{color:"red", size:12}}>*</span>Number of Seats</Label>}
                    name={numberOfSeats.name}
                    id={numberOfSeats.name}
                    placeholder="Number Of Seats"
                    autoComplete="numberOfSeats"/>
            </Col>
                
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={isThereAnAlarmSystem} 
                    label = {<Label htmlFor="alarmSystem"><span style={{color:"red", size:12}}>*</span>Does your car have an alarm system?</Label>}
                    name={vehicleHasAlarmSystem.name}
                    id={vehicleHasAlarmSystem.name}
                    value="value" text="text">
                </InputSelect>
            </Col>
        </FormGroup>





        <FormGroup row className="my-0">
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={isThereNightParking} 
                    label = {<Label htmlFor="nightParking"><span style={{color:"red", size:12}}>*</span>Do you have night parking at your place of residence?</Label>}
                    name={residentHasNightParking.name} 
                    id={residentHasNightParking.name}
                    value="value" text="text">
                </InputSelect>
            </Col>
                
            <Col  xs="12" sm="6"  lg="6">
                <InputSelect data={isVehiclesUsedByOthers}
                label = {<Label htmlFor="whoUsesVehicle"><span style={{color:"red", size:12}}>*</span>Is Vehicle Used by Others?</Label>}
                name={isVehicleUsedByOthers.name}
                id={isVehicleUsedByOthers.name} 
                value="value" text="text">
                </InputSelect>
            </Col>
        </FormGroup>
        
        

        </div>
    )
}
