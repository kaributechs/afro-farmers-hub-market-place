import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup, Label} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  OtherUserDetailsForm = (props)=> {
  const {
    formField: {

    otherVehicleUser_Role,
    otherVehicleUser_RoleDescription,
    otherVehicleUser_MaritalStatus,
    otherVehicleUser_FirstName,
    otherVehicleUser_LastName,
    otherVehicleUser_EmailAddress,
    otherVehicleUser_DateOfBirth,
    otherVehicleUser_PhoneNumber,
    otherVehicleUser_Gender,


    }
  } = props;

  const maritalStatuses = [
    {text: "Select your Marital Status", value:""},
      {text: "Single", value: "single"},
      {text: "Married", value: "married"},
      {text: "Widowed", value: "widowed"},
      {text: "Partnership", value: "partnership"},
      {text: "Other", value: "other"},
  ]

  const genders = [
    {text: "Select your Marital Status", value:""},
      {text: "Female", value: "female"},
      {text: "Male", value: "male"},
  ]


   return(

    <div className="animated fadeIn">

          <strong>Other Vehicle User Details</strong>
          <hr />


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="otherVehicleUser_NationalId"><span style={{color:"red", size:12}}>*</span>National Id/ Passport Number</Label>}
                              name="otherVehicleUser_NationalId"
                              id="otherVehicleUser_NationalId"
                              placeholder="National Id/ Passport Number"
                              autoComplete="national-id"/>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="otherVehicleUser_LicenseNumber"><span style={{color:"red", size:12}}>*</span>License Number</Label>}                              name="otherVehicleUser_LicenseNumber"
                              id="otherVehicleUser_LicenseNumber"
                              placeholder="License Number"/>
                      </Col>
                      </FormGroup>
<div><br /></div>

                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                          <InputField type="text"
                              label = {<Label htmlFor="otherVehicleUser_FirstName"><span style={{color:"red", size:12}}>*</span>First Name</Label>}                                name="otherVehicleUser_FirstName"
                                id="otherVehicleUser_FirstName"
                                placeholder="First Name"
                                autoComplete="first-name"/>
                        </Col>

                      <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                              label = {<Label htmlFor="otherVehicleUser_LastName"><span style={{color:"red", size:12}}>*</span>Last Name</Label>}                              name="otherVehicleUser_LastName"
                              id="otherVehicleUser_LastName"
                              placeholder="Last Name"
                              autoComplete="Last name"/>
                      </Col>
                    </FormGroup>

<div><br /></div>

                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <InputField type="driverEmailAddress"
                              label = {<Label htmlFor="otherVehicleUser_EmailAddresss"><span style={{color:"red", size:12}}>*</span>Email</Label>}                              name="otherVehicleUser_EmailAddress"
                              id="otherVehicleUser_EmailAddresss"
                              placeholder="Email"
                              autoComplete="email"/>
                   </FormGroup>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                            <InputField type="number"
                                  label = {<Label htmlFor="driverPhoneNumber"><span style={{color:"red", size:12}}>*</span>Phone Number</Label>}                                  name="otherVehicleUser_PhoneNumber"
                                  id="driverPhoneNumber"
                                  placeholder="Phone Number"/>
                        </Col>
                    </FormGroup>

<div><br /></div>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                            <InputField type="date"
                                  label = {<Label htmlFor="otherVehicleUser_DateOfBirth"><span style={{color:"red", size:12}}>*</span>Date of Birth</Label>}
                                  name="otherVehicleUser_DateOfBirth"
                                  id="otherVehicleUser_DateOfBirth"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-dateOfBirth"/>
                        </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={genders}
                                  name="otherVehicleUser_Gender"
                                  id="otherVehicleUser_Gender"
                                  label = {<Label htmlFor="otherVehicleUser_Gender"><span style={{color:"red", size:12}}>*</span>Gender</Label>}
                                  value="value" text="text"/>
                    </Col>
                    </FormGroup>

<div><br /></div>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={maritalStatuses}
                                  name="otherVehicleUser_MaritalStatus"
                                  id="otherVehicleUser_MaritalStatus"
                              label = {<Label htmlFor="otherVehicleUser_MaritalStatus"><span style={{color:"red", size:12}}>*</span>Marital Status</Label>}                                  value="value" text="text"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               label="Role of Applicant at the Company"
                               name="otherVehicleUser_Role"
                               id="otherVehicleUser_Role"
                               placeholder="driver Role"/>

                    </Col>
                 </FormGroup>

<div><br /></div>

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               rows={2}
                               label="Role Description"
                               name="otherVehicleUser_RoleDescription"
                               id="otherVehicleUser_RoleDescription"
                               placeholder="Role Description"/>
                    </Col>

        </FormGroup>



    </div>
  )
}
