import PersonalDetails from '../../PersonalDetails';
import applicantDetailFormModel from './applicationDetailFormModel';

const {
  formField: {

    PersonalDetails:{
    salutation,
    nationalId,
    clientFirstName,
    clientLastName,
    dateOfBirth,
    clientGender,
    maritalStatus,},


    ContactDetails:{
    clientEmailAddress,
    clientPhoneNumber,
    clientResidentialStreetAddress,
    clientResidentialCity,
    clientRegion,
    residentialZip,
    workAddressZip,
    clientCountry,
    clientWorkStreetAddress,
    clientWorkCity,
    workRegion,
    workCountry,},


    VehicleDetails:{
    vehicleRegistrationNumber,
    clientLicenseNumber,
    dateWhenLicenseWasObtained,
    vehicleMake,
    vehicleModel,
    vehicleYear,
    vehicleType,
    vehicleMileage,
    vehicleWorth,
    numberOfSeats,
    vehicleHasAlarmSystem,
    residentHasNightParking,
    isVehicleUsedByOthers,},


    SecondaryDriverDetails:{
    dependentLicenseNumber,
    dependentDateWhenLicenseWasObtained,
    dependentNationalId,
    relationshipToBeneficiary,
    dependentFirstName,
    dependentLastName,
    dependentDateOfBirth,
    dependentEmailAddress,
    dependentPhoneNumber,
    dependentGender,},


    InsuranceQuestions:{
    carInsuranceType,
    thirdPartyLiability,
    bearsAllRisks,
    interestedInCourtesyCar,
    coversTotalLoss,
    coversExcessCosts,
    periodOfInsuranceTo,
    periodOfInsuranceFrom,}

    
  }
} = applicantDetailFormModel;

export default {
  formField:{
    PersonalDetails:{
      [nationalId.name]: "",
      [clientFirstName.name]: "",
      [clientLastName.name]: "",
      [dateOfBirth.name]: "",
      [clientGender.name]: "",
      [salutation.name]:"",
      [maritalStatus.name]: "",},

    ContactDetails:{
      [clientEmailAddress.name]: "",
      [clientPhoneNumber.name]: "",
      [clientResidentialStreetAddress.name]: "",
      [clientResidentialCity.name]: "",
      [clientRegion.name]: "",
      [residentialZip.name]: "",
      [workAddressZip.name]: "",
      [clientCountry.name]: "",
      [clientWorkStreetAddress.name]: "",
      [clientWorkCity.name]: "",
      [workRegion.name]: "",
      [workCountry.name]:"",},

    VehicleDetails:{
      [vehicleRegistrationNumber.name]: "",
      [clientLicenseNumber.name]: "",
      [dateWhenLicenseWasObtained.name]:"",
      [vehicleMake.name]: "",
      [vehicleModel.name]: "",
      [vehicleYear.name]: "",
      [vehicleMileage.name]: "",
      [vehicleWorth.name]: "",
      [numberOfSeats.name]: "",
      [vehicleHasAlarmSystem.name]: "",
      [residentHasNightParking.name]: "",
      [isVehicleUsedByOthers.name]: "",},

    SecondaryDriverDetails:{
      [dependentLicenseNumber.name]: "",
      [dependentDateWhenLicenseWasObtained.name]:"",
      [dependentNationalId.name]: "",
      [relationshipToBeneficiary.name]: "",
      [dependentFirstName.name]: "",
      [dependentLastName.name]: "",
      [dependentEmailAddress.name]: "",
      [dependentDateOfBirth.name]: "",
      [dependentPhoneNumber.name]: "",
      [dependentGender.name]: "",},

    InsuranceQuestions:{
      [carInsuranceType.name]: "",
      [thirdPartyLiability.name]: "",
      [bearsAllRisks.name]: "",
      [interestedInCourtesyCar.name]: "",
      [coversTotalLoss.name]: "",
      [coversExcessCosts.name]: "",
      [periodOfInsuranceFrom.name]: "",
      [periodOfInsuranceTo.name]: "",}
  
  }
};


