
export default {
  formId: 'ApplicantDetailsForm',
  formField: {

  CompanyDetails: {
    companyDetails_clientCompanyClass: {
      name: "companyDetails_clientCompanyClass"
    },
    companyDetails_clientCompanyName: {
      name: "companyDetails_clientCompanyName"
    },
    companyDetails_clientRegistrationNumber: {
      name: "companyDetails_clientRegistrationNumber"
    },
    companyDetails_clientCIN: {
      name: "companyDetails_clientCIN"
    },
    companyDetails_clientBusinessDescription: {
      name: "companyDetails_clientBusinessDescription"
    },
},

    ContactDetails: {
    contactDetails_clientEmailAddress:{
      name: "contactDetails_clientEmailAddress"
    },
    contactDetails_clientPhoneNumber : {
      name: "contactDetails_clientPhoneNumber"
    },
    contactDetails_clientResidentialStreetAddress: {
      name: "contactDetails_clientResidentialStreetAddress"
    },
    contactDetails_clientResidentialCity: {
      name: "contactDetails_clientResidentialCity"
    },
    contactDetails_clientRegion: {
      name: "contactDetails_clientRegion"
    },
    contactDetails_workRegion: {
      name: "contactDetails_workRegion"
    },
    contactDetails_residentialZip: {
      name: "contactDetails_residentialZip"
    },
    contactDetails_workAddressZip: {
      name: "contactDetails_workAddressZip"
    },
    contactDetails_clientCountry: {
      name: "contactDetails_clientCountry"
    },
    contactDetails_workCountry: {
      name: "contactDetails_workCountry"
    },
    contactDetails_clientWorkStreetAddress: {
      name: "contactDetails_clientWorkStreetAddress"
    },
    contactDetails_clientWorkCity: {
      name: "contactDetails_clientWorkCity"
    },
},

    VehicleDetails: {
    vehicleDetails_RegistrationNumber: {
      name: "vehicleDetails_RegistrationNumber"
    },

    vehicleDetails_Make: {
      name: "vehicleDetails_Make"
    },
    vehicleDetails_Model: {
      name: "vehicleDetails_Model"
    },
    vehicleDetails_Year: {
      name: "vehicleDetails_Year"
    },
    vehicleDetails_Mileage: {
      name: "vehicleDetails_Mileage"
    },
    vehicleDetails_Worth: {
      name: "vehicleDetails_Worth"
    },
    vehicleDetails_Usage: {
      name: "vehicleDetails_Usage"
    },
    vehicleDetails_Type: {
      name: "  vehicleDetails_Type,"
    },
    vehicleDetails_numberOfSeats: {
      name: "vehicleDetails_numberOfSeats"
    },
    vehicleDetails_alarmSystemYesOrNo: {
      name: "vehicleDetails_alarmSystemYesOrNo"
    },
    vehicleDetails_nightParkingYesOrNo: {
      name: "vehicleDetails_nightParkingYesOrNo"
    },
    vehicleDetails_isVehicleUsedByOthers: {
      name: "vehicleDetails_isVehicleUsedByOthers"
    },
},
    OtherVehicleUser: {
    otherVehicleUser_LicenseNumber: {
      name: "otherVehicleUser_LicenseNumber"
    },
    otherVehicleUser_NationalId: {
      name: "otherVehicleUser_NationalId"
    },
    otherVehicleUser_FirstName: {
      name: "otherVehicleUser_FirstName"
    },
    otherVehicleUser_LastName: {
      name: "otherVehicleUser_LastName"
    },
    otherVehicleUser_EmailAddress: {
      name: "otherVehicleUser_EmailAddress"
    },
    otherVehicleUser_DateOfBirth: {
      name: "otherVehicleUser_DateOfBirth"
    },
    otherVehicleUser_PhoneNumber: {
      name: "otherVehicleUser_PhoneNumber"
    },
    otherVehicleUser_Gender: {
      name: "otherVehicleUser_Gender"
    },
    otherVehicleUser_MaritalStatus: {
      name: "otherVehicleUser_MaritalStatus"
     },
    otherVehicleUser_Role: {
      name: "otherVehicleUser_Role"
     },
    otherVehicleUser_RoleDescription: {
      name: "otherVehicleUser_RoleDescription"
    },
},

    InsuranceQuestions:  {
    insuranceQuestions_carInsuranceType: {
      name: "insuranceQuestions_carInsuranceType"
    },
    insuranceQuestions_thirdPartyLiability: {
      name: "insuranceQuestions_thirdPartyLiability"
    },
    insuranceQuestions_allRisksYesOrNo: {
      name: "insuranceQuestions_allRisksYesOrNo"
    },
    insuranceQuestions_replacementCarYesOrNo: {
      name: "insuranceQuestions_replacementCarYesOrNo"
    },
    insuranceQuestions_totalLossYesOrNo: {
      name: "insuranceQuestions_totalLossYesOrNo"
    },
    insuranceQuestions_excessYesOrNo: {
      name: "insuranceQuestions_excessYesOrNo"
    },
    insuranceQuestions_periodOfInsuranceFrom: {
      name: "insuranceQuestions_periodOfInsuranceFrom"
    },
    insuranceQuestions_periodOfInsuranceTo: {
      name: "insuranceQuestions_periodOfInsuranceTo"
    },
},

    VehicleUserDetails: {
    vehicleUserDetails_LastName: {
      name: "vehicleUserDetails_LastName"
    },
    vehicleUserDetails_FirstName: {
      name: "vehicleUserDetails_FirstName"
    },
    vehicleUserDetails_LicenseNumber: {
      name: "vehicleUserDetails_LicenseNumber"
    },
    vehicleUserDetails_NationalId: {
      name: "vehicleUserDetails_NationalId"
    },
    vehicleUserDetails_DateOfBirth: {
      name: "vehicleUserDetails_DateOfBirth"
    },
    vehicleUserDetails_EmailAddress: {
      name: "vehicleUserDetails_EmailAddress"
    },
    vehicleUserDetails_PhoneNumber: {
      name: "vehicleUserDetails_PhoneNumber"
    },
    vehicleUserDetails_Gender: {
      name: "vehicleUserDetails_Gender"},
    vehicleUserDetails_Role: {
      name: "vehicleUserDetails_Role"
    },
    vehicleUserDetails_RoleDescription: {
      name: "vehicleUserDetails_RoleDescription"
    },
    vehicleUserDetails_MaritalStatus: {
      name: "vehicleUserDetails_MaritalStatus"
    },
}


  }
};
