import React, {useState} from 'react';
import {Formik, Form} from 'formik';
import applicationDetailFormModel from "./FormModel/applicationDetailFormModel";
import formInitialValues from './FormModel/formInitialValues';
import validationSchema from "./FormModel/validationSchema";
import Stepper from "react-stepper-horizontal";
import {Button, Card, CardBody, CardHeader} from "reactstrap";
import {BusinessDetailsForm, ContactDetailsForm, DriverDetailsForm, OtherUserDetailsForm, VehicleDetailsForm, InsuranceQuestions, DocumentUpload} from "./Forms";
import {CButton, CCardFooter}                                                                         from "@coreui/react";
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from 'react-redux';
import {businessFormTask, contactFormTask, vehicleUserFormTask, vehicleFormTask, otherUserFormTask, insuranceFormTask} from 'src/store/actions';
import {personalDetailsTask} from 'src/store/actions';
import {contactDetailsTask} from 'src/store/actions'
import {vehicleDetailsTask} from 'src/store/actions'
import {secondaryDriverDetailsTask} from 'src/store/actions'
import {insuranceQuestionsTask} from 'src/store/actions'


const {formId, formField, PersonalDetails, ContactDetails, VehicleDetails, SecondaryDriverDetails} = applicationDetailFormModel;

function _renderStepContent(step) {
  switch (step) {
    case 0:
      return <BusinessDetailsForm formField={formField}/>;
    case 1:
      return <ContactDetailsForm formField={formField}/>;
    case 2:
      return <DriverDetailsForm formField={formField}/>;
    case 3:
      return <VehicleDetailsForm formField={formField}/>;
    case 4:
      return <OtherUserDetailsForm formField={formField}/>;
    case 5:
      return <InsuranceQuestions formField={formField}/>;
    case 6:
      return <DocumentUpload formField={formField}/>;
  }
}

export const BusinessVehicleInsuranceApplicationStepper = () => {

const dispatch = useDispatch()
const unfoldable = useSelector((state) => state.layout?.sidebarUnfoldable)
  const dispatch = useDispatch();
  const unfoldable = useSelector((state) => state.layout?.sidebarUnfoldable);

  const [activeStep, setActiveStep] = useState(0);
  const sections = [
    {title: 'Business Details', onClick: () => setActiveStep(0)},
    {title: 'Contact Details', onClick: () => setActiveStep(1)},
    {title: 'Vehicle User Details', onClick: () => setActiveStep(2)},
    {title: 'Vehicle Details', onClick: () => setActiveStep(3)},
    {title: 'Other Vehicle User Details', onClick: () => setActiveStep(4)},
    {title: 'Insurance Questions', onClick: () => setActiveStep(5)},
    {title: 'Document Upload', onClick: () => setActiveStep(6)},
  ];


  const currentValidationSchema = validationSchema[activeStep];
  const isLastStep = activeStep === sections.length - 1;


  async function _submitForm(values, actions) {

    alert(JSON.stringify(values, null, 2));
    actions.setSubmitting(false);

    setActiveStep(activeStep + 1);
  }

  function _handleSubmit(values, actions) {

    if(activeStep==1){
    dispatch(businessFormTask(values))
    }

    if(activeStep==2){
    dispatch(contactFormTask(values))
    }

    if(activeStep==1){
    dispatch(vehicleUserFormTask(values))
    }

    if(activeStep==1){
    dispatch(vehicleFormTask(values))
    }

    if(activeStep==1){
    dispatch(otherUserFormTask(values))
    }

    if(activeStep==1){
    dispatch(insuranceFormTask(values))
    }


    if(activeStep===1){
      dispatch(personalDetailsTask(values))
      console.log(values)
    }
    if(activeStep===2){
      dispatch(contactDetailsTask(values))
      console.log(values)
    }
    if(activeStep===3){
      dispatch(vehicleDetailsTask(values))
      console.log(values)
    }
    if(activeStep===4){
      dispatch(secondaryDriverDetailsTask(values))
      console.log(values)
    }
    if(activeStep===5){
      dispatch(insuranceQuestionsTask(values))
      console.log(values)
    }    
    if (isLastStep) {
      _submitForm(values, actions);
    } else {
      setActiveStep(activeStep + 1);
      actions.setTouched({});
      actions.setSubmitting(false);
    }
  }

  const prev = () => {
    setActiveStep(activeStep - 1);
  }

  function save(values, actions){
    if(activeStep===0){
      dispatch(personalDetailsTask(values))
      console.log(values)
    }
    if(activeStep===1){
      dispatch(contactDetailsTask(values))
      console.log(values)
    }
    if(activeStep===2){
      dispatch(vehicleDetailsTask(values))
      console.log(values)
    }
    if(activeStep===3){
      dispatch(secondaryDriverDetailsTask(values))
      console.log(values)
    }
    if(activeStep===4){
      dispatch(insuranceQuestionsTask(values))
      console.log(values)
    }    
    else{
      actions.setSubmitting(false);
    }
  }


  return (
    <React.Fragment>
      <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Card>

        <CardHeader>
        <h2>Vehicle Insurance Application</h2>
          <Stepper
            steps={sections}
            activeStep={activeStep}
            activeColor="grey"
            defaultBarColor="grey"
            completeColor="#4dbd74"
            completeBarColor="grey"
          />

          <br/>
        </CardHeader>

        {activeStep === sections.length ? (
          <BusinessDetailsForm/>
        ) : (
          <Formik
            initialValues={formInitialValues}
            validationSchema={currentValidationSchema}
            onSubmit={_handleSubmit}

          >
            {({
                isSubmitting
              }) => (
              <Form id={formId}>
                <CardBody>
                  {_renderStepContent(activeStep)}
                </CardBody>
                <CCardFooter>
                  <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    {activeStep !== 0 ? (
                      <CButton className=" mb-3" onClick={prev}>Back</CButton>
                    ) : <span></span>}

                    <CButton className=" mb-3" onClick={save}>Save</CButton>  
                    <CButton className=" mb-3" type="submit">
                      {isLastStep ? 'Submit' : 'Next'}
                    </CButton>

                  </div>
                </CCardFooter>
              </Form>
            )}
          </Formik>

        )}
      </Card>
    </React.Fragment>
  );
}
