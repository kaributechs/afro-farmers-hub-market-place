import React, { useState } from 'react';
import {  Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    clientEmailAddress: Yup.string()
    .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    .email('Invalid email address')
    .required('Email is required!'),
    dateOfBirth: Yup.string()
    .required('Date of Birth Is Required'),
    clientPhoneNumber: Yup.string()
    .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    .max(10, 'Phone number should be at least 10 characters')
    .required('Phone Number is required'),
    clientResidentialStreetAddress: Yup.string()
    .required('Address Line One Required!'),
    clientResidentialCity: Yup.string()
    .required('Address Line Two Required!'),
    clientRegion: Yup.string()
    .required('Region is Required!'),
    residentialZip: Yup.string()
    .required('Zip is Required!')
    .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/),
    clientCountry: Yup.string()
    .required('Client Country is Required!'),
    clientWorkStreetAddress: Yup.string()
    .required('Address Line One Required!'),
    clientWorkCity: Yup.string()
    .required('Address Line Two Required!'),
    workAddressZip: Yup.string()
    .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/)
    .required('Please select zip!'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}


const onSubmit = (values, { setSubmitting }) => {

  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const ContactDetails = ({contactDetails}) =>{

  const [country, setCountry] = useState('')

  const [region, setRegion] = useState('')

  const [workCountry, setWorkCountry] = useState('')

  const [workRegion, setWorkRegion] = useState('')


  const handleCountryChange = (country) => {
    setCountry(country);
  }

  const handleRegionChange = (region) => {
    setRegion(region);
  }

  const handleWorkCountryChange = (workCountry) => {
    setWorkCountry(workCountry);
  }

  const handleWorkRegionChange = (workRegion) => {
    setWorkRegion(workRegion);
  }

/*

  const [selected, setSelected] = useState('');

 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        clientEmailAddress: true,
        clientPhoneNumber: true,
        clientResidentialStreetAddress:true,
        clientResidentialCity:true,
        clientRegion:true,
        residentialZip:true,
        clientCountry:true,
        clientWorkStreetAddress:true,
        clientWorkCity:true,
        workAddressZip: true,

      }
    )
    validateForm(errors)
  }*/
  return(

    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>

      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Contact Information</strong>
          <hr />
          <Formik
            initialValues={contactDetails}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit
              }) => (

                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>

                    <FormGroup  row className="my-0">
                      <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="email"
                              name="clientEmailAddress"
                              id="clientEmailAddress"
                              placeholder="Email"
                              autoComplete="email"
                              valid={!errors.clientEmailAddress}
                              invalid={touched.clientEmailAddress && !!errors.clientEmailAddress}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientEmailAddress} />
                        <FormFeedback>{errors.clientEmailAddress}</FormFeedback>
                      </FormGroup>
                      </Col>

                      <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="clientPhoneNumber">Phone Number</Label>
                            <Input type="text"
                                  name="clientPhoneNumber"
                                  id="clientPhoneNumber"
                                  placeholder="Phone Number"
                                  autoComplete="phone-number"
                                  valid={!errors.clientPhoneNumber}
                                  invalid={touched.clientPhoneNumber && !!errors.clientPhoneNumber}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.clientPhoneNumber} />
                            <FormFeedback>{errors.clientPhoneNumber}</FormFeedback>
                          </FormGroup>
                        </Col>
                    </FormGroup>

                    <hr />

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                        <strong>Residential Address</strong>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <strong>Work Address</strong>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <FormGroup>
                      <Label for="clientResidentialStreetAddress">Street Address</Label>
                        <Input type="textarea"
                              name="clientResidentialStreetAddress"
                              id="clientResidentialStreetAddress"
                              placeholder="Street Address"
                              autoComplete="clientResidentialStreetAddress"
                              valid={!errors.clientResidentialStreetAddress}
                              invalid={touched.clientResidentialStreetAddress && !!errors.clientResidentialStreetAddress}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientResidentialStreetAddress} />
                      <FormFeedback>{errors.clientResidentialStreetAddress}</FormFeedback>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="clientWorkStreetAddress">Street Address</Label>
                      <Input type="textarea"
                            name="clientWorkStreetAddress"
                            id="clientWorkStreetAddress"
                            placeholder="Street Address"
                            autoComplete="clientWorkStreetAddress"
                            valid={!errors.clientWorkStreetAddress}
                            invalid={touched.clientWorkStreetAddress && !!errors.clientWorkStreetAddress}
                            autoFocus={false}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.clientWorkStreetAddress} />
                    <FormFeedback>{errors.clientWorkStreetAddress}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="clientResidentialCity">City</Label>
                        <Input type="text"
                              name="clientResidentialCity"
                              id="clientResidentialCity"
                              placeholder="City"
                              autoComplete="clientResidentialCity"
                              valid={!errors.clientResidentialCity}
                              invalid={touched.clientResidentialCity && !!errors.clientResidentialCity}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientResidentialCity} />
                      <FormFeedback>{errors.clientResidentialCity}</FormFeedback>
                      </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Label for="clientWorkCity">City</Label>
                        <Input type="text"
                              name="clientWorkCity"
                              id="clientWorkCity"
                              placeholder="City"
                              autoComplete="clientWorkCity"
                              valid={!errors.clientWorkCity}
                              invalid={touched.clientWorkCity && !!errors.clientWorkCity}
                              autoFocus={false}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientWorkCity} />
                      <FormFeedback>{errors.clientWorkCity}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="country">Country</Label>
                    <div style={{borderColor: 'green'}}>
                      <CountryDropdown
                        value={country}
                        onChange={handleCountryChange}/>
                    </div>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="workCountry">Country</Label>
                      <CountryDropdown
                        value={workCountry}
                        onChange={handleWorkCountryChange}/>
                    </FormGroup>
                    </Col>
                    </FormGroup>



                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="Region">Region</Label>
                      <RegionDropdown
                        country={country}
                        value={region}
                        onChange={handleRegionChange} />
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="workRegion">Region</Label>
                      <RegionDropdown
                        country={workCountry}
                        value={workRegion}
                        onChange={handleWorkRegionChange} />
                    </FormGroup>
                    </Col>
                    </FormGroup>



                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="residentialZip">Zip Code</Label>
                      <Input type="text"
                            name="residentialZip"
                            id="residentialZip"
                            placeholder="Enter Zip Code"
                            autoComplete="residentialZip"
                            valid={!errors.residentialZip}
                            invalid={touched.residentialZip && !!errors.residentialZip}
                            autoFocus={false}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.residentialZip} />
                    <FormFeedback>{errors.residentialZip}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="workAddressZip">Zip Code</Label>
                      <Input type="text"
                            name="workAddressZip"
                            id="workAddressZip"
                            placeholder="Enter Zip Code"
                            autoComplete="workAddressZip"
                            valid={!errors.workAddressZip}
                            invalid={touched.workAddressZip && !!errors.workAddressZip}
                            autoFocus={false}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.workAddressZip} />
                    <FormFeedback>{errors.workAddressZip}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    {/* <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="country">Country</Label>
                    <ReactFlagsSelect
                      selected={selected}
                      onSelect={code => setSelected(code)}
                    />
                    <FormFeedback>{errors.residentialZip}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                    <Label for="country">Country</Label>
                    <ReactFlagsSelect
                      selected={selected}
                      onSelect={code => setSelected(code)}
                    />
                    <FormFeedback>{errors.residentialZip}</FormFeedback>
                    </FormGroup>
                    </Col>
                    </FormGroup> */}


                    </Form>


              )} />
        </CardBody>
      </Card>



  </div>
  )
}

export default ContactDetails;
