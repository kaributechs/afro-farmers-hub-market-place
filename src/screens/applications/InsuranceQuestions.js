import React from 'react';
import {  Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    carInsuranceType: Yup.string()
    .required('Type of Car Insurance is Required!'),
    thirdPartyLiability: Yup.string()
    .required('Third Party Liability is Required!'),
    bearsAllRisks: Yup.string()
    .required('This field is Required!'),
    interestedInCourtesyCar: Yup.string()
    .required('This field is Required!'),
    coversTotalLoss: Yup.string()
    .required('This field is Required!'),
    coversExcessCosts: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceFrom: Yup.string()
    .required('This field is Required!'),
    periodOfInsuranceTo: Yup.string()
    .required('This field is Required!'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  carInsuranceType:"",
  thirdPartyLiability:"",
  bearsAllRisks:"",
  interestedInCourtesyCar:"",
  coversTotalLoss:"",
  coversExcessCosts:"",
  periodOfInsuranceFrom:"",
  periodOfInsuranceTo:"",

}

const onSubmit = (values, { setSubmitting }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const InsuranceQuestions = () =>{


  
/*
 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        carInsuranceType:true,
        thirdPartyLiability:true,
        bearsAllRisks:true,
        interestedInCourtesyCar:true,
        coversTotalLoss:true,
        coversExcessCosts:true,
        periodOfInsuranceFrom:true,
        periodOfInsuranceTo:true,
      
      }
    )
    validateForm(errors)
  }*/
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Insurance Questions</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit

              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>
                    
                
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="carInsuranceType">Type of Car Insurance</Label>
                        <Input type="select" name="carInsuranceType" id="carInsuranceType"
                        valid={!errors.carInsuranceType}
                        invalid={touched.carInsuranceType && !!errors.carInsuranceType}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.carInsuranceType} >
                          <option value="" disabled selected hidden>Select Coverage</option> 
                          <option value="liabilityCoverage">Liability Coverage</option>
                          <option value="collisionCoverage">Collision Coverage</option>
                          <option value="comprehensiveCoverage">Comprehensive Coverage</option>
                          <option value="personalInjuryProtection">Personal Injury Protection</option>
                        </Input>
                        <FormFeedback>{errors.carInsuranceType}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="thirdPartyLiability">Third Party Insurance</Label>
                        <Input type="select" name="thirdPartyLiability" id="thirdPartyLiability"
                        valid={!errors.thirdPartyLiability}
                        invalid={touched.thirdPartyLiability && !!errors.thirdPartyLiability}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.thirdPartyLiability} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                        </Input>
                        <FormFeedback>{errors.thirdPartyLiability}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="bearsAllRisks">All Risks</Label>
                        <Input type="select" name="bearsAllRisks" id="bearsAllRisks"
                        valid={!errors.bearsAllRisks}
                        invalid={touched.bearsAllRisks && !!errors.bearsAllRisks}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.bearsAllRisks} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.bearsAllRisks}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="interestedInCourtesyCar">Would You Want A Courtesy Car?</Label>
                        <Input type="select" name="interestedInCourtesyCar" id="interestedInCourtesyCar"
                        valid={!errors.interestedInCourtesyCar}
                        invalid={touched.interestedInCourtesyCar && !!errors.interestedInCourtesyCar}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.interestedInCourtesyCar} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.interestedInCourtesyCar}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="coversTotalLoss">Total Loss</Label>
                        <Input type="select" name="coversTotalLoss" id="coversTotalLoss"
                        valid={!errors.coversTotalLoss}
                        invalid={touched.coversTotalLoss && !!errors.coversTotalLoss}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.coversTotalLoss} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.coversTotalLoss}</FormFeedback>
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <Label htmlFor="coversExcessCosts">Excess</Label>
                        <Input type="select" name="coversExcessCosts" id="coversExcessCosts"
                        valid={!errors.coversExcessCosts}
                        invalid={touched.coversExcessCosts && !!errors.coversExcessCosts}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.coversExcessCosts} >
                          <option value="" disabled selected hidden>Select</option> 
                          <option value="yes">yes</option>
                          <option value="no">no</option>
                        </Input>
                        <FormFeedback>{errors.coversExcessCosts}</FormFeedback>
                      </FormGroup>
                    </Col>
                    </FormGroup>


                    <strong>Period of Insurance</strong>
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                          <FormGroup>
                            <Label for="periodOfInsuranceFrom">From:</Label>
                            <Input type="date"
                                  name="periodOfInsuranceFrom"
                                  id="periodOfInsuranceFrom"
                                  placeholder="yyyy/mm/dd"
                                  autoComplete="new-periodOfInsuranceFrom"
                                  valid={!errors.periodOfInsuranceFrom}
                                  invalid={touched.periodOfInsuranceFrom && !!errors.periodOfInsuranceFrom}
                                  required
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.periodOfInsuranceFrom} />
                            <FormFeedback>{errors.periodOfInsuranceFrom}</FormFeedback>
                          </FormGroup>
                        </Col>
                      
                        <Col  xs="12" sm="6"  lg="6">
                        <FormGroup>
                          <Label for="periodOfInsuranceTo">To:</Label>
                          <Input type="date"
                                name="periodOfInsuranceTo"
                                id="periodOfInsuranceTo"
                                placeholder="yyyy/mm/dd"
                                autoComplete="new-periodOfInsuranceTo"
                                valid={!errors.periodOfInsuranceTo}
                                invalid={touched.periodOfInsuranceTo && !!errors.periodOfInsuranceTo}
                                required
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.periodOfInsuranceTo} />
                          <FormFeedback>{errors.periodOfInsuranceTo}</FormFeedback>
                        </FormGroup>
                      </Col>
                    </FormGroup>

                    
                    </Form>

                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default InsuranceQuestions;
