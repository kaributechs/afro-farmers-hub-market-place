import React, {useState} from "react";
import {Link} from 'react-router-dom'
import { useForm } from "react-hook-form";
import {Col, FormGroup, Label, Card,CardHeader,CardBody,CardFooter, Row} from "reactstrap";
import axios from "axios";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import './style.css'
import {CButton, CFormSelect, CLink} from "@coreui/react";
import ScreensCards from "../../components/cards/ScreensCards";

const schema = yup.object().shape({
 insuranceType: yup
   .string()
   .required("Required!")
   .min(6, "Too Short")
   .max(15, "Too Long"),
});

const InsuranceTypeScreen = () => {
  const {insuranceType} = useState("null");
 const {
   register,
   handleSubmit,
   formState: { errors },
 } = useForm({
   resolver: yupResolver(schema),
 });

 const onSubmit = async (data) => {

   const formData = new FormData();

   formData.append("insuranceType", `${data.insuranceType}`);

   const response = await axios.post(`/api/v1/request-claim`, formData);

   console.log(data)
   console.log("**Post Response Data**", response.data);
 };

  return (
    <div>
     <Link to="/new" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <Row>
        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Vehicle Insurance"}
            footerText={"Apply"}
            cardRoute="new/type/details"/>
        </Col>

        <Col xs="12" sm="6" lg="3">
          <ScreensCards
            cardTitle={"Health Insurance"}
            footerText={"Apply"}
            cardRoute="new/type"/>
        </Col>
      </Row>
    </div>
  );
};

export default InsuranceTypeScreen;

