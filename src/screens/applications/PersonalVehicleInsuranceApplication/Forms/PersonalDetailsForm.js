import {Link} from "react-router-dom";
import {Card, CardBody, CardHeader, Col, FormGroup, Label} from "reactstrap";
import React from "react";
import {InputField, InputSelect} from "../../FormFields";



export  const  PersonalDetailsForm = (props)=> {
  const {
    formField: {
      PersonalDetails:{
        nationalId,
        maritalStatus,
        clientFirstName,
        clientLastName,
        dateOfBirth,
        clientGender,
        salutation
      }
    }
  } = props;

  const maritalStatuses = [
    {text: "Select Your Marital Status", value:""},
      {text: "Single", value: "single"},
      {text: "Married", value: "married"},
      {text: "Widowed", value: "widowed"},
      {text: "Partnership", value: "partnership"},
      {text: "Other", value: "other"},
  ]

  const genders = [
    {text: "Select Your Gender", value:""},
      {text: "Male", value: "male"},
      {text: "Female", value: "female"},
      {text: "Other", value: "other"},
  ]

  const salutations = [
    {text: "Select your Salutation", value:""},
      {text: "Mr", value: "mr"},
      {text: "Mrs", value: "mrs"},
      {text: "Miss", value: "miss"},
      {text: "Ms", value: "ms"},
      {text: "Other", value: "other"},
  ]

   return(

    <div className="animated fadeIn">

          <strong>Personal Details</strong>
          <hr />
                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                         <InputField type="text"
                               name={nationalId.name}
                               id={nationalId.name}
                               label = {<Label htmlFor="nationalId"><span style={{color:"red", size:12}}>*</span>National Id/ Passport Number</Label>}
                               placeholder="National Id/ Passport Number"/>

                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                     <InputSelect data={salutations}
                                  name={salutation.name}
                                  id={salutation.name}
                                  label = {<Label htmlFor="salutations"><span style={{color:"red", size:12}}>*</span>Salutation</Label>}
                                  value="value" text="text"/>
                    </Col>
                  </FormGroup>



                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                          name={clientFirstName.name}
                          id={clientFirstName.name}
                          label = {<Label htmlFor="firstName"><span style={{color:"red", size:12}}>*</span>First Name</Label>}
                          placeholder="First Name"
                          autoComplete="first-name"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                        <InputField type="text"
                          name={clientLastName.name}
                          id={clientLastName.name}
                          label = {<Label htmlFor="lastName"><span style={{color:"red", size:12}}>*</span>Last Name</Label>}
                          placeholder="Last Name"
                          autoComplete="username" />
                    </Col>
                  </FormGroup>




                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                        <InputField type="date"
                          name={dateOfBirth.name}
                          id={dateOfBirth.name}
                          label = {<Label htmlFor="clientDateOfBirth"><span style={{color:"red", size:12}}>*</span>Date of Birth</Label>}
                          placeholder="Password"
                          autoComplete="new-dateOfBirth"/>
                    </Col>

                    <Col  xs="12" sm="6"  lg="6">
                      <InputSelect data={genders}
                        name={clientGender.name}
                        id={clientGender.name}
                        label = {<Label htmlFor="clientGender"><span style={{color:"red", size:12}}>*</span>Gender</Label>}
                        value="value" text="text"
                        autoComplete="gender"/>
                    </Col>
                </FormGroup>

                  

                {/*<FormGroup row className="my-0">
                  <Col  xs="12" sm="6"  lg="6">
                    <InputSelect data={genders}
                      name={clientGender.name}
                      id={clientGender.name}
                      label = {<Label htmlFor="clientGender"><span style={{color:"red", size:12}}>*</span>Gender</Label>}
                      value="value" text="text"/>
                  </Col>

                  <Col  xs="12" sm="6"  lg="6">
                  <InputField type="date"
                    name={dateOfBirth.name}
                    id={dateOfBirth.name}
                    label = {<Label htmlFor="clientDateOfBirth"><span style={{color:"red", size:12}}>*</span>Date of Birth</Label>}
                    placeholder="Password"
                    autoComplete="new-dateOfBirth"/>
                  </Col>
   </FormGroup>*/}



                  <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                      <InputSelect data={maritalStatuses}
                          name={maritalStatus.name}
                          id={maritalStatus.name}
                          label = {<Label htmlFor="clientMaritalStatus"><span style={{color:"red", size:12}}>*</span>Marital Status</Label>}
                          value="value" text="text"
                          autoComplete="maritalStatus"/>
                    </Col>
                  </FormGroup>


    </div>
  )

}
