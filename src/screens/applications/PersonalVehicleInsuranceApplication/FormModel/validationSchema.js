import * as Yup from 'yup';
import applicationDetailFormModel from "./applicationDetailFormModel";
const {
  formField: {
    PersonalDetails:{
      nationalId,
      clientFirstName,
      clientLastName,
      dateOfBirth,
      clientGender,
      salutation,
      maritalStatus,},
  
  
      ContactDetails:{
      clientEmailAddress,
      clientPhoneNumber,
      clientResidentialStreetAddress,
      clientResidentialCity,
      clientRegion,
      residentialZip,
      workAddressZip,
      clientCountry,
      clientWorkStreetAddress,
      clientWorkCity,
      workRegion,
      workCountry,},
  
  
      VehicleDetails:{
      vehicleRegistrationNumber,
      clientLicenseNumber,
      dateWhenLicenseWasObtained,
      vehicleMake,
      vehicleModel,
      vehicleYear,
      vehicleMileage,
      vehicleWorth,
      numberOfSeats,
      vehicleHasAlarmSystem,
      residentHasNightParking,
      isVehicleUsedByOthers,},
  
  
      SecondaryDriverDetails:{
      dependentLicenseNumber,
      dependentDateWhenLicenseWasObtained,
      dependentNationalId,
      relationshipToBeneficiary,
      dependentFirstName,
      dependentLastName,
      dependentEmailAddress,
      dependentDateOfBirth,
      dependentPhoneNumber,
      dependentGender,},
  
  
      InsuranceQuestions:{
      carInsuranceType,
      thirdPartyLiability,
      bearsAllRisks,
      interestedInCourtesyCar,
      coversTotalLoss,
      coversExcessCosts,
      periodOfInsuranceTo,
      periodOfInsuranceFrom,}
    
  }
} = applicationDetailFormModel;


export default [
  Yup.object().shape({
    [nationalId.name]: Yup.string()
      .matches(/^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/, 'Please Enter A Valid ID Number')
      .min(8, `National ID has to be at least 14 characters`)
      .required('National ID is required'),
    [salutation.name]: Yup.string()
      .required('Please select a salutaion!'),
    [clientFirstName.name]: Yup.string()
      .min(2, `First Name has to be at least 2 characters`)
      .required('First Name is required'),
    [clientLastName.name]: Yup.string()
      .min(2, `Last Name has to be at least 2 characters `)
      .required('Last Name is required'),
    [dateOfBirth.name]: Yup.string()
      .required('Date of Birth Is Required'),
    [clientGender.name]: Yup.string()
      .required('Gender is required!'),
    [maritalStatus.name]: Yup.string()
      .required("Marital status is required")

    }),



    Yup.object().shape({
      [clientEmailAddress.name]: Yup.string()
        .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, 'An email addresss muct contain the following: "@", ".".')
        .email('Invalid email address ')
        .required('Email address is required!'),
      [clientPhoneNumber.name]: Yup.string()
        .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'This phone number does not exist!')
        .max(10, 'Phone number should be at least 10 characters')
        .required('Phone Number is required'),
      [clientResidentialStreetAddress.name]: Yup.string()
        .required('Street Address Required!'),
      [clientResidentialCity.name]: Yup.string()
        .required('City is Required!'),

      /*[clientRegion.name]: Yup.string()
        .required('Region is required!'),*/

      [residentialZip.name]: Yup.string()
        .required('Zip is Required!')
        .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/, 'Invalid Zip Code!'),

      /*[clientCountry.name]: Yup.string()
        .required('Client Country is Required!'),*/

      [clientWorkStreetAddress.name]: Yup.string()
        .required('Street Address Required!'),
      [clientWorkCity.name]: Yup.string()
        .required('City is Required!'),
      [workAddressZip.name]: Yup.string()
        .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/, 'Invalid Zip Code!')
        .required('Please select zip!'),

      /*[workRegion.name]: Yup.string()
        .required('Region is required!'),
      [workCountry.name]: Yup.string()
        .required('Country is Required')*/
    }),



    Yup.object().shape({
      [vehicleRegistrationNumber.name]: Yup.string()
        .required('Vehicle Registration Number is Required!'),
      [dateWhenLicenseWasObtained.name]: Yup.string()
        .required('Date is required'),
      [clientLicenseNumber.name]: Yup.string()
        .required('Client License Number is Required!'),
      [vehicleMake.name]: Yup.string()
        .required('Vehicle Make is Required!'),
      [vehicleModel.name]: Yup.string()
        .required('Vehicle Model is Required!'),
      [vehicleYear.name]: Yup.string()
        .required('Vehicle Year is Required!'),
      [vehicleMileage.name]: Yup.string()
        .required('Vehicle Mileage is Required!'),
      [vehicleWorth.name]: Yup.string()
        .required('Vehicle Worth is Required!'),
      [numberOfSeats.name]: Yup.string()
        .required('This field is Required!'),
      [vehicleHasAlarmSystem.name]: Yup.string()
        .required('This field is Required!'),
      [residentHasNightParking.name]: Yup.string()
        .required('This field is Required!'),
      [isVehicleUsedByOthers.name]: Yup.string()
        .required('This field is Required!'),
    }),



    Yup.object().shape({
      [dependentNationalId.name]: Yup.string()
        .matches(/^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/, 'Please Enter A Valid ID Number')
        .min(8, `National ID has to be at least 14 characters`)
        .required('National ID is required'),
      [dependentDateWhenLicenseWasObtained.name]: Yup.string()
        .required('Please Select the date when you got your license.'),
      [dependentLicenseNumber.name]: Yup.string()
        .required('License Number is Required!'),
      [relationshipToBeneficiary.name]: Yup.string()
        .required('Relationship to Beneficiary is Required!'),
      [dependentFirstName.name]: Yup.string()
        .min(2, `First Name has to be at least 2 characters`)
        .required('First Name is required'),
      [dependentLastName.name]: Yup.string()
        .min(2, `Last Name has to be at least 2 characters `)
        .required('Last Name is required'),
      [dependentEmailAddress.name]: Yup.string()
        .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, 'Invalid email address!')
        .email('Invalid email address')
        .required('Email is required!'),
      [dependentDateOfBirth.name]: Yup.string()
        .required('Date of Birth Is Required'),
      [dependentPhoneNumber.name]: Yup.string()
        .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'Invalid Phone Number!')
        .max(10, 'Phone number should be at least 10 characters')
        .required('Phone Number is required'),
      [dependentGender.name]: Yup.string()
        .required('Gender is required!'),
    }),



    Yup.object().shape({
    [carInsuranceType.name]: Yup.string()
      .required('Type of Car Insurance is Required!'),
    [thirdPartyLiability.name]: Yup.string()
      .required('Third Party Liability is Required!'),
    [bearsAllRisks.name]: Yup.string()
      .required('This field is Required!'),
    [interestedInCourtesyCar.name]: Yup.string()
      .required('This field is Required!'),
    [coversTotalLoss.name]: Yup.string()
      .required('This field is Required!'),
    [coversExcessCosts.name]: Yup.string()
      .required('This field is Required!'),
    [periodOfInsuranceFrom.name]: Yup.string()
      .required('This field is Required!'),
      //.max(Date(Date.now()), 'Vehicle Year Invalid'),
    [periodOfInsuranceTo.name]: Yup.string()
      .required('This field is Required!'),
    })

];
