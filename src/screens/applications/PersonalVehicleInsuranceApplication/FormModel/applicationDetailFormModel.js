
export default {
  formId: 'ApplicantDetailsForm',

  formField: {

        //PersonalDetails
      PersonalDetails:{
        nationalId: {
          name: "nationalId"
        },
        salutation:{
          name: "salutation"
        },
        clientFirstName: {
          name: "clientFirstName"
        },
        clientLastName: {
          name: "clientLastName"
        },
        dateOfBirth: {
          name: "dateOfBirth"
        },
        clientGender: {
          name: "clientGender"
        },
        maritalStatus: {
          name: "maritalStatus"
        },},


        //ContactDetails
      ContactDetails:{
        clientEmailAddress:{
          name: "lientEmailAddress"
        },
        clientPhoneNumber : {
          name: "clientPhoneNumber"
        },
        clientResidentialStreetAddress: {
          name: "clientResidentialStreetAddress"
        },
        clientResidentialCity: {
          name: "clientResidentialCity"
        },
        clientRegion: {
          name: "clientRegion"
        },
        clientCountry: {
          name: "clientCountry"
        },
        residentialZip: {
          name: "residentialZip"
        },
        clientWorkStreetAddress: {
          name: "clientWorkStreetAddress"
        },
        clientWorkCity: {
          name: "clientWorkCity"
        },
        workRegion: {
          name: "workRegion"
        },
        workCountry: {
          name: "workCountry"
        },
        workAddressZip: {
          name: "workAddressZip"
        },},

        
        //VehicleDetails
        VehicleDetails:{
        clientLicenseNumber: {
          name: "clientLicenseNumber"
        },
        dateWhenLicenseWasObtained:{
          name: "dateWhenLicenseWasObtained"
        },
        vehicleRegistrationNumber: {
          name: "vehicleRegistrationNumber"
        },
        vehicleMake: {
          name: "vehicleMake"
        },
        vehicleModel: {
          name: "vehicleModel"
        },
        vehicleYear: {
          name: "vehicleYear"
        },
        vehicleMileage: {
          name: "vehicleMileage"
        },
        vehicleWorth: {
          name: "vehicleWorth"
        },
        numberOfSeats: {
          name: "numberOfSeats"
        },
        vehicleHasAlarmSystem: {
          name: "vehicleHasAlarmSystem"
        },
        residentHasNightParking: {
          name: "residentHasNightParking"
        },
        isVehicleUsedByOthers: {
          name: "isVehicleUsedByOthers"
        },},


        //SecondaryDriverInformation
      SecondaryDriverDetails:{
        dependentNationalId: {
          name: "dependentNationalId"
        },
        relationshipToBeneficiary: {
          name: "relationshipToBeneficiary"
        },
        dependentFirstName: {
          name: "dependentFirstName"
        },
        dependentLastName: {
          name: "dependentLastName"
        },
        dependentEmailAddress: {
          name: "dependentEmailAddress"
        },
        dependentDateOfBirth: {
          name: "dependentDateOfBirth"
        },
        dependentPhoneNumber: {
          name: "dependentPhoneNumber"
        },
        dependentGender: {
          name: "dependentGender"
        },
        dependentLicenseNumber: {
          name: "dependentLicenseNumber"
        },
        dependentDateWhenLicenseWasObtained:{
          name: "dependentDateWhenLicenseWasObtained"
        },},



        //InsuranceQuestions
      InsuranceQuestions:{
        carInsuranceType: {
          name: "carInsuranceType"
        },
        thirdPartyLiability: {
          name: "thirdPartyLiability"
        },
        bearsAllRisks: {
          name: "bearsAllRisks"
        },
        interestedInCourtesyCar: {
          name: "interestedInCourtesyCar"
        },
        coversTotalLoss: {
          name: "coversTotalLoss"
        },
        coversExcessCosts: {
          name: "coversExcessCosts"
        },
        periodOfInsuranceFrom: {
          name: "periodOfInsuranceFrom"
        },
        periodOfInsuranceTo: {
          name: "periodOfInsuranceTo"
        },},
  }
};
