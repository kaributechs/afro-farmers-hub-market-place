import React from 'react';
import {useField} from 'formik';
import {CFormLabel} from "@coreui/react";
import {FormFeedback, FormGroup} from "reactstrap";
import {CountryDropdown} from "react-country-region-selector";

export function InputCountryDropdown(props) {
  const {name, label, ...rest} = props;
  const [field, meta, helper] = useField(props);
  const {setValue} = helper;

  const _onChange = (event) => {
    debugger;
    setValue(event);
  }
  return (
    <React.Fragment>
      <FormGroup>
        <CFormLabel htmlFor={name}>{label}</CFormLabel>
        <CountryDropdown
          className=" form-control mb-3"
          onChange={_onChange}
          {...field}
          {...rest}
        />
        <FormFeedback>{meta.error}</FormFeedback>
      </FormGroup>
    </React.Fragment>

  );
}
