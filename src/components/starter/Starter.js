import React from 'react'
import useTrans from '../../hooks/useTrans'
const Starter = () => {

    // eslint-disable-next-line
    const[t, handleClick] = useTrans();
    return (
        <React.Fragment>
            <div className="container">
                <div className="jumbotron" style={{ backgroundColor: 'grey' }}>
                    <h1 className="display-4">{t("Header1.1")}</h1>
                    <p className="lead">{t("HeaderSubtitle1.1")}</p>
                    <hr className="my-4" />
                    <p>{t("HeaderSubtitle2.1")}</p>
                    <p className="lead">
                        <a className="btn btn-primary btn-lg" href="website.com" role="button">{t("LearnMore.1")}</a>
                    </p>
                </div>
            </div>

        </React.Fragment>
    )
}

export default Starter
