import 'core-js'
import 'react-app-polyfill/stable'
import axios from 'axios'
import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./i18next";
import * as serviceWorker from "./serviceWorker";
import store from './store'
import { Provider } from 'react-redux'
import { icons } from './assets/icons'

React.icons = icons

ReactDOM.render(
  <Provider store={store}>
    <Suspense fallback="<div>loading~~~</div>">
        <App />
    </Suspense>
  </Provider>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();


